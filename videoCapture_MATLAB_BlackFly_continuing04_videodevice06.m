
is_draw_occupied = false;

camera_1_pool_edge=[ 380,820,1;	380,780,1;	380,740,1;	380,700,1;	380,660,1;	380,620,1;	380,580,1;	380,540,1;	420,820,1;	420,780,1;	420,740,1;	420,700,1;	420,660,1;	420,620,1;	420,580,1;	420,540,1;	460,580,1;	460,540,1;	460,500,1;	500,540,1;	500,500,1;	500,460,1;	540,540,1;	540,500,1;	540,460,1; ];

% if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('gige', 1, 'RGB8Packed');  end
% if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('gige', 2, 'RGB8Packed');  end
setup_videoDevice()
if ~exist('videoDevice','var') , videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  end
if ~exist('camera_params','var') , camera_params = load_camera_params(); end

cycleHz = 3;
cyclePeriod = 1/cycleHz;                 %   0.1seconds per processing cycle
robotCommandPeriodMs=1000; %1.5s
robotVelocityLeft=10; 
robotVelocityRight=6;
motionRegionsMinimumArea=25;
largeMotionRegionMinimumArea=100;
max_robot_LED_diff = 150;
renderRectifiedImageInvHz = 10;          % render the rectified image every whatever frames; 0 or less to not render
delayTriggerCyclePeriod = cyclePeriod - 0.001;   % sensitivity to cycle length / how much faith we have in the system clock
imageOld=zeros(3,3,3); imageOld(:,:,:) = [];
homography_Matrix_ = homography_calculate_and_apply();

% WinVideo 
    [firstFrame , metadata] = step(videoDevice);
% %     firstFrame = rot90(firstFrame,2);
    firstFrame = undistortImage(firstFrame,camera_params,'OutputView','full');    
     camera_1_xmin=10;
     camera_1_xmax=1650;
     camera_1_ymin=160;
     camera_1_ymax=915;
     firstFrameGoodBits = firstFrame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:); 
% % %firstFrameGoodBits = firstFrame;
%     rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'full'  );
    firstFrame = firstFrameGoodBits;
%     rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200  );
%     rectifiedImage = flipud(rectifiedImage);
%     firstFrame = rectifiedImage;
imageOld = firstFrame;  

midpoint = [ size(firstFrame,2)/2 , size(firstFrame,1)/2 ];
diff_image = zeros(size(firstFrame,1),size(firstFrame,2));

numRobotsToTrack=10;

maxRobotSat = 0.3;
minRobotLuminosity = 0.95;
minRobotSize = 3;
maxRobotLEDSize = 500;


robotLocationBase = [-1.00, -1.00]; % robotLocationBase = midpoint; %clear('robotLocation')
robotLocation = robotLocationBase;
for robot_num=1:numRobotsToTrack-1
    robotLocation = [ robotLocation ; robotLocationBase ];
end
robotLocationHistoryRow = robotLocation;
robotLocationHistory = zeros(2,numRobotsToTrack,2,'double');    % camera x,y / u,v
robotLocationHistory(1,:,:) = robotLocationHistoryRow; 
led_vs_track_history = zeros(2,numRobotsToTrack,2,'uint8');     % LED number - from regions spotted in the image - to the robot track being traced
robotStateHistory = zeros(2,numRobotsToTrack,4,'double');       % x,y,theta,vel


playLoop=true;
liveImageFigureHandle = figure('NumberTitle','on','Name', 'live image', 'Position',[0 490 510 328 ]);
% figure(liveImageFigureHandle); imshow(firstFrame,'Border','tight'); drawnow;
% figure(occupiedFigHandle); imshow(firstFrame,'Border','tight'); drawnow;
% uicontrol('Style', 'pushbutton', 'Callback', 'uiwait(gcbf)');
 
 
camera_1_background_image_  = camera_1_background_image(videoDevice , homography_Matrix_);
image(camera_1_background_image_);  
hold on;
drawnow;

frame_num=0;
beforeLoopTic=tic;
% while playLoop && frame_num<40 &&  tic-beforeLoopTic < 10000000

radiusOscUdpConnection = udp('169.254.99.218',9000);
fopen(radiusOscUdpConnection);
jaredOscUdpConnection = udp('169.254.21.113',9000);
fopen(jaredOscUdpConnection);

while playLoop 
    frame_num = frame_num+1;
    
    if exist('is_draw_occupied','var') && is_draw_occupied && (~exist('occupiedFigHandle','var') || ~isvalid(occupiedFigHandle)), occupiedFigHandle = figure('NumberTitle','on','Name', 'occupied image', 'Position',[0 490+510 510 328 ]); end
    
    robotLocationHistory(frame_num+1, :, :) = robotLocationHistoryRow; 
    timerval=tic;    
                                                                                                
    [firstFrame , metadata] = step(videoDevice);                                                
% %     firstFrame = rot90(firstFrame,2);
    firstFrame = undistortImage(firstFrame,camera_params,'OutputView','full');                     
     camera_1_xmin=10;
     camera_1_xmax=1650;
     camera_1_ymin=160;
     camera_1_ymax=915;
     firstFrameGoodBits = firstFrame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:); 
% % %     firstFrameGoodBits = firstFrame;
    firstFrame = firstFrameGoodBits;
%     imshow(firstFrame);
    hsvImg = rgb2hsv(firstFrame);
%     imshow(imshow);
    whiteGlowingBin = hsvImg(:,:,2) < maxRobotSat & hsvImg(:,:,3) > minRobotLuminosity;
    whiteGlowingBin = bwmorph(whiteGlowingBin, 'close'); % Morphological closing to take care of some of the noisy thresholding - http://stackoverflow.com/questions/28004426/segment-pixels-in-an-image-based-on-colour-matlab
%     whiteGlowingBin(1:size(whiteGlowingBin,1), 1:350)=0;
%     whiteGlowingBin(1:115, 350:825)=0;
%     whiteGlowingBin(1:size(whiteGlowingBin,1), 825:size(whiteGlowingBin,2))=0;
%     whiteGlowingBin(475:size(whiteGlowingBin,1), 350:825)=0;
    figure(liveImageFigureHandle); image(whiteGlowingBin);  drawnow;   
    LEDs = regionprops(whiteGlowingBin, 'Area', 'Centroid', 'BoundingBox');% Use regionprops to filter based on area, return location of green blocks
    LEDs(vertcat(LEDs.Area) < minRobotSize) = []; % Remove every region smaller than minRegionSize
    LEDs(vertcat(LEDs.Area) > maxRobotLEDSize) = [];
        
%     figure(liveImageFigureHandle); imshow(firstFrame);  drawnow;   
        
        %% occupied / free space / obstacles image
%         figure(occupiedFigHandle); 
        occupiedSpace     = firstFrame - camera_1_background_image_; 
%         occupiedSpaceGrayGpu = gpuArray(rgb2gray(occupiedSpace));     % to GPU array
        occupiedSpace_gray     = rgb2gray(occupiedSpace);
        occupiedSpace_medfilt2 = medfilt2(occupiedSpace_gray, [3 3]);   % remove noise
        occupiedSpace_medfilt2_twice = medfilt2(occupiedSpace_medfilt2, [3 3]);   % remove noise
%         occupiedSpaceGray = gather(occupiedSpaceGrayGpu);             % return from GPU array
        occupiedSpaceBw   = im2bw(occupiedSpace_medfilt2_twice, 0.05);             % convert to binary 
        occupiedSpaceBw2  = occupiedSpaceBw;
        if is_draw_occupied , figure(occupiedFigHandle); image(occupiedSpaceBw); drawnow;    end;
        
        %% optical flow image
        if ~isempty(imageOld) % if we have a frame from t-1, start taking differences for optical flow
            diff_image  = firstFrame - imageOld;
            diff_image  = rgb2gray(diff_image);
            imageOld    = firstFrame;                 
            diff_image  = medfilt2(diff_image, [3 3]);   % remove noise
            diff_image  = medfilt2(diff_image, [3 3]);    % remove noise
            diff_image_bw = im2bw(diff_image, 0.02); % convert to 
%             figure(3000); imshow(diff_image_bw);
        end
        
        %% robots
        points=zeros(size(LEDs,1),2);
        if length(LEDs) <1
            continue;
        elseif length(LEDs) <2
            robotLocation_now = [cluster_average_x, cluster_max_y-20]; 
            world__ = homography_Matrix_*[ robotLocation_now(1) , robotLocation_now(2) , 1 ]';
            world__ = world__.*(1/world__(3));            
            display(sprintf('/robot/%d/state/%f | %f | %f | %f' , robot_number , world__(1) , world__(2) , 0.00 , 0.00));
            oscsend2(jaredOscUdpConnection,sprintf('/robot/%d/state/' , robot_number) , 'ffff' , world__(1) , world__(2) , 0.00 , 0.00 );           
        else
            for propNum_=1:size(LEDs,1)
% % %                  if ~exist('plothandle_','var') || ~isvalid(plothandle_), plothandle_ = plot( LEDs(propNum_).Centroid(1),LEDs(propNum_).Centroid(2), 'rx','MarkerSize',20); 
% % %                  else, set(plothandle_,'XData',[200,100,400])
% % %                  end; drawnow;   
        %         subplot(2,2,1); 
                 plot( LEDs(propNum_).Centroid(1),LEDs(propNum_).Centroid(2), 'rx','MarkerSize',20);  
                 drawnow;

                    points(propNum_,1) = LEDs(propNum_).Centroid(1);
                    points(propNum_,2) = LEDs(propNum_).Centroid(2);             
            end
                
            figure(liveImageFigureHandle); hold on; 
            if ~exist('plothandle_','var') || ~isvalid(plothandle_), plothandle_ = plot( LEDs(propNum_).Centroid(1),LEDs(propNum_).Centroid(2), 'rx','MarkerSize',20); 
            else set(plothandle_,'XData',points(:,1)); set(plothandle_,'YData',points(:,2));
            end; drawnow;   
        %         end
                LEDs_in_rows = zeros(size(LEDs,1), 2);
                for propNum_=size(LEDs,1):-1:1
                    LEDs_in_rows(propNum_,1) = LEDs(propNum_).Centroid(1);
                    LEDs_in_rows(propNum_,2) = LEDs(propNum_).Centroid(2);
                end
                nearestLEDs = robotCorners( points, size(firstFrame,2), size(firstFrame,1), max_robot_LED_diff); 
                props_used = [];
                robotNumber = 0;
                %% assign the robot number - consistency of numbering assumes that entries in robotRegionProposals are in consistent order - presumably by coordinate order
                LED_coordinates = zeros(size(LEDs,1),2);
                for i = 1:size(LEDs,1)
                    LED_coordinates(i,1)=LEDs(i).Centroid(1);
                    LED_coordinates(i,2)=LEDs(i).Centroid(2);
                end;
        %         if(LED_coordinates)
        %         distances_ = pdist(LED_coordinates);
                clusters_indices = clusterdata( LED_coordinates , 'criterion' ,  'distance' , 'distance' , 'euclidean' , 'cutoff' , max_robot_LED_diff );


                for i = 1:max(unique(clusters_indices))
                    robot_number = i;
                    %  average coordinates = sum(LED_coordinates(clusters_indices == i, :)) / size(LED_coordinates(clusters_indices == i),1)  % group 1
                    cluster_average_x   = sum(LED_coordinates(clusters_indices == i, 1)) / size(LED_coordinates(clusters_indices == i),1);
                    cluster_max_y       = max(LED_coordinates(clusters_indices == i, 2));
                    robotLocation_now = [cluster_average_x, cluster_max_y-20];                     
                    world__ = homography_Matrix_*[ robotLocation_now(1) , robotLocation_now(2) , 1 ]';
                    world__ = world__.*(1/world__(3));            
                    display(sprintf('/robot/%d/state/%f | %f | %f | %f' , robot_number , world__(1) , world__(2) , 0.00 , 0.00));
                    oscsend2(jaredOscUdpConnection,sprintf('/robot/%d/state/' , robot_number) , 'ffff' , world__(1) , world__(2) , 0.00 , 0.00 );           
                    figure(liveImageFigureHandle); hold on;
                    plot( [min(LED_coordinates(clusters_indices == i, 1)),max(LED_coordinates(clusters_indices == i, 1)),max(LED_coordinates(clusters_indices == i, 1)),min(LED_coordinates(clusters_indices == i, 1)),min(LED_coordinates(clusters_indices == i, 1)) ] , ...
                       [max(LED_coordinates(clusters_indices == i, 2)),max(LED_coordinates(clusters_indices == i, 2)),min(LED_coordinates(clusters_indices == i, 2)),min(LED_coordinates(clusters_indices == i, 2)),max(LED_coordinates(clusters_indices == i, 2)) ]);
                    drawnow;
                end
        end
                
        
        %{
        for propNum_=size(LEDs,1):-1:1
            if sum(props_used==propNum_) > 0 , continue; end;	% skip if this LED has already been used
            robotNumber = robotNumber+1;
            nearestLEDsFound = nearestLEDs(propNum_,(nearestLEDs(propNum_,:) > 0));
%             nearestNeighbours_ = robotCornersA( [robotRegionProposals(propNum_).Centroid(1),robotRegionProposals(propNum_).Centroid(2)] , points, size(firstFrame,2), size(firstFrame,1) );
            xsum=0;  ysum=0;  xcount=0;  ycount=0;           
            for i = 1:size(nearestLEDsFound,2)-1  
                props_used=[ props_used nearestLEDsFound(i) nearestLEDsFound(i+1) ];
%                 plot(...
%                     [LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(1),LEDs(nearestLEDs(nearestLEDsFound(i+1))).Centroid(1)], ...                    
%                     [LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(2),LEDs(nearestLEDs(nearestLEDsFound(i+1))).Centroid(2)], ...
%                     'c' );
                xsum=xsum+LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(1);
                ysum=ysum+LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(2);
                xcount=xcount+1;
                ycount=ycount+1;
            end            
            xsum=xsum+LEDs(nearestLEDs(nearestLEDsFound(size(nearestLEDsFound,2)))).Centroid(1);
            ysum=ysum+LEDs(nearestLEDs(nearestLEDsFound(size(nearestLEDsFound,2)))).Centroid(2);
            xcount=xcount+1;
            ycount=ycount+1;
            xmean=xsum/xcount;
            xcount=0;
            ymean=ysum/ycount;
            ycount=0;
            robotLocation_now=[xmean,ymean];
            if 1 == frame_num 
                robotLocation(robotNumber,:)=robotLocation_now;
                robotLocationHistory(frame_num,robotNumber,:)=robotLocation_now;                 
            else
                euc_dists = pdist2(robotLocation_now,squeeze(robotLocationHistory(frame_num-1,:,:)));
                [ min_euc_dist, min_euc_dist_idx] = min(euc_dists);
                robot_number = min_euc_dist_idx; 
                robotLocation(robot_number,:) = robotLocation_now;
                robotLocationHistory(frame_num,robot_number,:) = robotLocation_now;
                robotLocation_then = robotLocationHistory(frame_num-1,robot_number,:);
                position_diff = robotLocation_now - shiftdim( robotLocation_then , 1);
                vel = norm(position_diff,2);
                if vel < 0.2  % TODO not moving fast, so may just be rotating and LEDs dis/appear so centre jumps and theta jumps, but no guage for rotation without motion commands
                    theta = robotStateHistory(frame_num-1,robot_number,3);
                else
                    theta = atan2(position_diff(2), position_diff(1));
                end
                robotStateHistory(frame_num,robot_number,:) = [ xmean , ymean , theta , vel];
%                 display( sprintf('/robot/%d/track = %d' , robotNumber , robot_number));  
                world__ = homography_Matrix_*[ xmean , ymean , 1 ]';
                world__ = world__.*(1/world__(3));
%                 oscsend2(jaredOscUdpConnection,sprintf('/robot/%d/state/',robot_number),'ffff',robotStateHistory(frame_num,robot_number,1),robotStateHistory(frame_num,robot_number,2),robotStateHistory(frame_num,robot_number,3),robotStateHistory(frame_num,robot_number,4));           
display(sprintf('/robot/%d/state/%f | %f | %f | %f',robot_number,world__(1),world__(2),robotStateHistory(frame_num,robot_number,3),robotStateHistory(frame_num,robot_number,4)));
                oscsend2(jaredOscUdpConnection,sprintf('/robot/%d/state/',robot_number),'ffff',world__(1),world__(2),robotStateHistory(frame_num,robot_number,3),robotStateHistory(frame_num,robot_number,4));           
                led_vs_track_history(frame_num,robotNumber,1) = propNum_;                             
                led_vs_track_history(frame_num,robotNumber,2) = robot_number;
            end
            
            %% plot zones around the robots
%             figure(liveImageFigureHandle); hold on; 
%                 plot(xmean,ymean,'rx','MarkerSize',50); plot(xmean,ymean,'go','MarkerSize',50);  plot(xmean,ymean,'go','MarkerSize',100); plot(xmean,ymean,'go','MarkerSize',150);   
%             drawnow;
            
            %% OSC for Yanto - robot 'radius' from centre (0) to edge (1);             
%             max_distance=sqrt(( size(occupiedSpaceBw,1)/2)^2+(size(occupiedSpaceBw,2)/2)^2); % TODO - move out of loop
            max_distance=sqrt((1200/2)^2+(1550/2)^2);            
            robot_xy_in_world=homography_Matrix_*[xmean,ymean,1]';
            robot_xy_in_world=robot_xy_in_world*(1/robot_xy_in_world(3));
            distance_from_centre = sqrt((1200/2 -robot_xy_in_world(1))^2+(1550/2-robot_xy_in_world(2))^2); % TODO - use real room centre, or real pool centre, rather than image centre
            distance_from_centre = distance_from_centre/max_distance;
            if distance_from_centre > 1 , distance_from_centre = 0.999; end;
            display( sprintf('/robot/%d/radius = %d' , robotNumber , distance_from_centre));
            oscsend2(radiusOscUdpConnection,sprintf('/robot/%d/radius',robotNumber),'f',distance_from_centre);
            
            
            %% OSC for Jared - distance to nearest occupied space - currently in pixels; 
            ymean_int=uint32(ymean);
            xmean_int=uint32(xmean);
            env_y_min=uint32(upper_limit(ymean_int+25,size(occupiedSpaceBw,1))); 
            env_y_max=uint32(lower_limit(ymean-150,1));                             % inverted
            env_x_min=uint32(lower_limit(xmean_int-25,1));
            env_x_max=uint32(upper_limit(xmean_int+25,size(occupiedSpaceBw,2)));
            occupiedSpaceBw(env_y_min:env_y_max, env_x_min:env_x_max)=0;
            if is_draw_occupied, figure(occupiedFigHandle); hold on;  
                 plot([xmean_int-25, xmean_int-25, xmean_int+25, xmean_int+25],[ymean_int+25,ymean_int-150,ymean_int-150,ymean_int+25],'c');
            end;
%                 plot(xmean,ymean,'rx','MarkerSize',50 ); plot(xmean,ymean,'go','MarkerSize',50); plot(xmean,ymean,'go','MarkerSize',100); plot(xmean,ymean,'go','MarkerSize',150);            
%             drawnow;
            
%             try using burrs num_burr_rays = 16;  angle=(2*pi)/num_burr_rays;  for  i=1:num_burr_rays, 1=1; end;
                % TODO TODO - homography or scale to angle 
             % bounds for robot silohette - remove the robot from the occupied space image
             % TODO - given the robot state, work out the shape and remove that rather than a whole section
             robot_pix_x_min=uint32(xmean)-40; if robot_pix_x_min<1, robot_pix_x_min=1; end;
             robot_pix_x_max=uint32(xmean)+40; if robot_pix_x_max>size(diff_image_bw,2) , robot_pix_x_max = size(diff_image_bw,2); end;
             robot_pix_y_min=uint32(ymean)-110; if robot_pix_y_min<1, robot_pix_y_min=1; end;
             robot_pix_y_max=uint32(ymean)+30; if robot_pix_y_max>size(diff_image_bw,1) , robot_pix_y_max = size(diff_image_bw,1); end;
%              plot([robot_pix_x_min, robot_pix_x_min, robot_pix_x_max ,robot_pix_x_max ], ...
%                  [robot_pix_y_min , robot_pix_y_max, robot_pix_y_max, robot_pix_y_min],'c')
%              figure(occupiedFigHandle); hold on; 
occupiedSpaceBw( robot_pix_y_min:robot_pix_y_max , robot_pix_x_min:robot_pix_x_max ) = false;
             


             for distance=40:5:200  %  TODO TODO - distance to object that is not the robot - need to identify the robot shape and exclude, and/or use the freespace 
                 image_section = [];
                 zone_pix_x_min=uint32(xmean)-distance; if zone_pix_x_min<1, zone_pix_x_min=1; end;
                 zone_pix_x_max=uint32(xmean)+distance; if zone_pix_x_max>size(diff_image_bw,2) , zone_pix_x_max = size(diff_image_bw,2); end;
                 zone_pix_y_min=uint32(ymean)-distance; if zone_pix_y_min<1, zone_pix_y_min=1; end;
                 zone_pix_y_max=uint32(ymean)+distance; if zone_pix_y_max>size(diff_image_bw,1) , zone_pix_y_max = size(diff_image_bw,1); end;
                 image_section = occupiedSpaceBw( zone_pix_y_min:zone_pix_y_max , zone_pix_x_min:zone_pix_x_max );
%                  image_section( image_section_robot_y_min:image_section_robot_y_max , image_section_robot_x_min:image_section_robot_x_max ) = false;
                 if sum(sum(image_section,1),2)>20
%                     display(sprintf('robot %d got an occupied region at %d.', propNum_,distance));
%                     display( sprintf('/robot/%d/occupied = %d' , robotNumber , distance));
%                     oscsend2(jaredOscUdpConnection,sprintf('/robot/%d/occupied',robotNumber),'i',distance);
                    break;
                 end
             end
        end            
        %}
        
        %%
%         if 1 == 0 
%         if renderRectifiedImageInvHz > 0 && 0 == mod(frame_num,renderRectifiedImageInvHz) % rectify image and display it            
% %             rectifiedImage = homwarp(homography_Matrix_, firstFrame, 'dimension', 1200);  
%             for propNum_=1:size(LEDs,1)
%                 robotCoords = [ LEDs(propNum_).Centroid(1) , LEDs(propNum_).Centroid(2) , 1];
%                 rectifiedRobotCoords = inv(homography_Matrix_)*robotCoords';rectifiedRobotCoords=rectifiedRobotCoords.*(1/rectifiedRobotCoords(3));
%             end
%             %%
%         end       
%         end
    
        
    %% Robot states for Jared to highlight
    
    
    %% Occupied areas for Jared to highlight 
%     for u = 1:100:size(occupiedSpaceBw,2)-100
%         for v = 1:100:size(occupiedSpaceBw,1)-100
%             if sum(sum(occupiedSpaceBw(v:v+100, u:u+100),2),1) > 2
% %                 oscsend2(jaredOscUdpConnection,sprintf('/occupied_space/'),'ii',u,v);           
%             end                
%         end
%     end


% % % %     for i=1:size(camera_1_pool_edge)
% % % %         pix = homography_Matrix_\camera_1_pool_edge(i,:)'; % projected_midpoint = inv(homography_Matrix_)*[600,750,1]';
% % % %         pix = pix.*(1/pix(3));      

% % % % %         figure(occupiedFigHandle);
% % % % %         image(firstFrame); hold on;
% % % %         if sum(sum(occupiedSpaceBw2(uint64(pix(1))-20:uint64(pix(1))+20, uint64(pix(2))-20:uint64(pix(2))+20),2),1) > 2
% % % % %             hold on; plot(pix(1),pix(2),'rx'); drawnow;  
% % % % %             oscsend2(jaredOscUdpConnection,sprintf('/occupied_space/'),'iii',camera_1_pool_edge(1),camera_1_pool_edge(2),1);  
% % % %         else
% % % % %             hold on; plot(pix(1),pix(2),'go'); drawnow;  
% % % % %             oscsend2(jaredOscUdpConnection,sprintf('/occupied_space/'),'iii',camera_1_pool_edge(1),camera_1_pool_edge(2),0);  
% % % %         end
% % % %     end

    
    elapsedTime = toc(timerval);   
    if elapsedTime < delayTriggerCyclePeriod , pause(cyclePeriod-elapsedTime); end; 
end


% release(videoDevice);



%%
%{ 
== Status ==
Merge with video_flow_extract1

% TODO - UDP NMEA to Yanto - radius from centre, nonlinear on deadspot, capped near walls
% TODO - UDP NMEA to Jared - x,y,theta,v 
% TODO - XBee to robot control vLeft, vRight
% TODO - XBee to robot servos d1,d2,d3
% TODO - free space detection
% TODO - path planning
% TODO - motion control to path
% TODO - swap out control to hand controller
% TODO - LED control
% TODO - 
% TODO - 
% TODO - human motion blobs
% TODO - UDP NMEA to Jared - motion blob relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - human motion characterisation
% TODO - UDP NMEA to Jared - motion characteristics relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - 
% TODO - different personalities for each robot
% TODO - 
% TODO - alter emotions based on interactions - persistent effects of
interaction aspects + drive to path
% TODO - alter emotions based on point in script
% TODO - 
% TODO - 
%  TODO - mask out the Kinect and other light sources with mid-grey before normalising.
%  TODO - normalise the image intensity before detecting - if performance works out.


== Status ==
Get frames from BlackFly as WinVideo in a loop with loop rate set by
cyclePeriod. 
Improved since previous because the imaq.VideoDevice doesn't tank like the
VideoInput does.

%}

%%

% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
% imaqreset;          % clear the decks
% imageAcquisitionInfo=imaqhwinfo;
% winvideoInfo = imaqhwinfo('winvideo'); winvideoInfoDeviceInfo=winvideoInfo.DeviceInfo; winvideoInfoDeviceInfo1=winvideoInfo.DeviceInfo(1) ; winvideoInfoDeviceInfo2=winvideoInfo.DeviceInfo(2) ; winvideoInfoDeviceInfo2imaqhwinfo=imaqhwinfo('winvideo',2);
% gigeInfo = imaqhwinfo('gige'); gigeInfoDeviceInfo=gigeInfo.DeviceInfo; gigeInfoDeviceInfo1=gigeInfo.DeviceInfo(1);
