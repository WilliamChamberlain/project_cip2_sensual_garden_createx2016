
frame0=getVideoFrames();
frame1=getVideoFrames(5);
frame2=getVideoFrames(5);
frame3=getVideoFrames(5);

figure; imshow( movieFrames(105).cdata(:,:,1) - movieFrames(103).cdata(:,:,1) )

figure; imshow( (movieFrames(105).cdata(:,:,1) - movieFrames(103).cdata(:,:,1)) - (movieFrames(104).cdata(:,:,1) - movieFrames(102).cdata(:,:,1)) )

A = [ 3 4 ; 3 4];
B = [ 1 5 ; 1 5 ];
C = ((A+B) + abs(A-B))/2

movieFrames = loadVideoFramesFromFile('/mnt/nixbig/data/project_CIP2_Sensual_Garden_CreateX2016/VREP_sim_vids/0008.avi',50)

movieFrames = loadVideoFramesFromFile('/mnt/nixbig/data/project_CIP2_Sensual_Garden_CreateX2016/VREP_sim_vids/0008.avi', 1000)

a = movieFrames(1).cdata(:,:,:);

erosionStructure = strel('square',5);
% erosionStructure = kcircle(3);

size(movieFrames(1).cdata,1)
size(movieFrames(1).cdata,2)

imageDiffs = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
imageDiffs2 = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
imageDiffsSummed = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
figure;
hold on
for i=1:size(movieFrames,2)
    if i > 1
%         imageDiffs(:,:,i-1)=  medfilt2( movieFrames(i).cdata(:,:,1) - movieFrames(i-1).cdata(:,:,1) );
        imageDiffs(:,:,i-1)=  medfilt2( sum(movieFrames(i).cdata,3) - sum(movieFrames(i-1).cdata,3) );
        imageDiffs(:,:,i-1)=medfilt2(imageDiffs(:,:,i-1), [2 10 ]);
        imageDiffs(:,:,i-1)=medfilt2(imageDiffs(:,:,i-1), [10 2]);
%         imshow( imageDiffs(:,:,i-1) );
%         pause(0.01);
    end
    if i > 2
%         imageDiffs2(:,:,i-2)=  medfilt2( movieFrames(i).cdata(:,:,1) - movieFrames(i-2).cdata(:,:,1)  );
        imageDiffs2(:,:,i-2)=  medfilt2( sum(movieFrames(i).cdata,3) - sum(movieFrames(i-2).cdata,3) );
        imageDiffsSummed(:,:,i-2)=  imageDiffs2(:,:,i-2)+imageDiffs(:,:,i-1);
%         imageDiffsSummed(:,:,i-2)=imerode(imageDiffsSummed(:,:,i-2),erosionStructure);
        imageDiffsSummed(:,:,i-2)=medfilt2(imageDiffsSummed(:,:,i-2), [2 10 ]);
        imageDiffsSummed(:,:,i-2)=medfilt2(imageDiffsSummed(:,:,i-2), [10 2]);
%         imageDiffsSummed(:,:,i-2)=  imageDiffs(:,:,i-2)+imageDiffs(:,:,i-1);
% %         imshow( arrayfun( @(x) ((x+255) + abs(x-255))/2 , imageDiffs.Summed(:,:,i-2) ) );
%         im = imageDiffsSummed(:,:,i-2);
%         opened = iopen(im, erosionStructure);
%         closed = iclose(opened, erosionStructure);
%         imbw = im2bw(imageDiffsSummed(:,:,i-2) , 0.05);
        imshow( imageDiffsSummed(:,:,i-2) );
%         imshow( closed > 10 );
        pause(0.01);
    end
end
hold off

