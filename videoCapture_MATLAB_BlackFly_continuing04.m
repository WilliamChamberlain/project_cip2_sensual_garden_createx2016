% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
imaqreset;          % clear the decks
imaqhwinfo
winvideoInfo = imaqhwinfo('winvideo')
winvideoInfo.DeviceInfo
winvideoInfo.DeviceInfo(1)
winvideoInfo.DeviceInfo(2)
imaqhwinfo('winvideo',2)

gigeInfo = imaqhwinfo('gige')
gigeInfo.DeviceInfo
gigeInfo.DeviceInfo(1)

% vid = videoinput('winvideo', 2, 'RGB32_1288x728');

vid = videoinput('gige', 1, 'RGB8Packed');
% vid = videoinput('gige', 1, 'Mono8');

% hvpc = vision.VideoPlayer;   %create video player object - sets up a window for display

src = getselectedsource(vid); 
framesPerTrigger = vid.FramesPerTrigger
vid.FramesPerTrigger = 1                        % TODO - external triggering from this script? - http://www.roborealm.com/forum/index.php?thread_id=5758 - https://www.ptgrey.com/KB/10250 - http://www.ptgrey.com/support/downloads/10113/ 
vid.FramesPerTrigger
% vid.FramesPerTrigger =1;  % defaults to 10 fro the BlackFly
vid.TriggerRepeat = Inf;                        % TODO - external triggering from this script? - http://www.roborealm.com/forum/index.php?thread_id=5758 - https://www.ptgrey.com/KB/10250 - http://www.ptgrey.com/support/downloads/10113/ 
vid.TriggerRepeat 
% vid.ReturnedColorspace = 'rgb';
% vid.ReturnedColorspace = 'grayscale'
% src.FrameRate = '30.0000';   %see propinfo(src,'FrameRate')
imaqhelp(vid)
frame = getsnapshot(vid);                       %  TODO  -  The DeviceLinkThroughputLimit property could not be set  -  http://www.ptgrey.com/KB/10350
figure(2147483646);   image(frame);
get(vid)                    % generic Matlab Video input object properties http://au.mathworks.com/help/imaq/examples/working-with-properties.html
get(src)                    % srouce object specific properties
propInfo_src = propinfo(src);
propInfo_vid = propinfo(vid);
% propinfo(src,'FrameRate')
% propinfo(vid,'FrameRate')
% propInfo_src.AcquisitionFrameRateAuto.
% propInfo_src.AcquisitionFrameRateAuto.Constraint
propinfo(src,'AcquisitionFrameRate')    %       stop(vid) vid.running
set(src,'AcquisitionFrameRateEnabled')
% set(src,'AcquisitionFrameRate',20)
imaqhelp(vid,'FrameGrabInterval')
propinfo(src,'FrameGrabInterval')
propinfo(vid,'FrameGrabInterval')
get(vid,'FrameGrabInterval')
vid.FrameGrabInterval
get(vid, 'FrameGrabInterval')
%get(src,'FrameGrabInterval')
get(src,'VideoMode')
src.VideoMode
imaqhelp(src,'VideoMode')
% enumerate values
set(src,'VideoMode') 
set(vid,'FrameGrabInterval')
% set(src,'FrameGrabInterval') 
% set(vid,'FrameGrabInterval',2)
% set(vid,'FrameGrabInterval',1)

homography_Matrix_ = homography_calculate_and_apply();

start(vid) 
vid.running
% get this warning: 
%   Warning: DeviceLinkThroughputLimit property adjusted to 46472000 by the camera.

%start main loop for image acquisition
while get(vid,'FramesAvailable')<1  %Wait until at least 1 frame is available
    try
        unavailable=1;
    catch ME
        display(ME)
    end
end
% hvpc.hide
% hvpc.show
figure(9000);
p = plot([0,1],'HitTest','off');
title('Click in figure to quit');
imageOld=zeros(3,3,3);
imageOld(:,:,:) = [];

for i = 1:1000
%   imgO=getdata(vid,1,'uint8');    %get image from camera
    try
        display(vid.FramesAvailable);    
        imgO=getdata(vid, 1);%get image from camera
    catch ME
        display(ME)
    end
 %   for i = 1:size(imgO,4)
    %    hvpc.step(imgO(:,:,:,i) );    %see current image in player
    %     imWarped = homwarp(H, imgO(:,:,:,i), 'dimension', 1200);
        firstFrame = imgO(:,:,:,1);
        figure(1); image(firstFrame);
        if 0 == mod(i,5) 
            imWarped = homwarp(homography_Matrix_, firstFrame, 'dimension', 1200);
            figure(2); image(imWarped);
        end
        if ~isempty(imageOld)
    %         imageDiff = imgO(:,:,:,i) - imageOld;
            imageDiff = firstFrame - imageOld;
            figure(3); image(imageDiff);
        end
    %     imageOld = imgO(:,:,:,i);
        imageOld = firstFrame;
%     end
  
    if gco ~= p
        stop(vid)  
        break;
    end
    
end
stop(vid);

% release(vid)
vid.FramesAvailable

vid.ReturnedColorspace = 'mono';

stop(vid)

% delete(src)
clear('src')
clear('vid')
delete(hvpc)
clear('hvpc') 
clear('gigeInfo') 
clear('winvideoInfo')
clear('imgO')

%  imaqreset;          % clear the decks


frame = getsnapshot(vid);
image(frame);
frame=flip(frame,1); frame=flip(frame,2); image(frame);
frame = getsnapshot(vid);frame=flip(frame,1); frame=flip(frame,2); image(frame);
frame=imadjust(frame); image(frame);
srgb2lab = makecform('srgb2lab');
lab2srgb = makecform('lab2srgb');
frame=applycform(frame, srgb2lab);
max_luminosity = 100;
L = frame(:,:,1)/max_luminosity;
frame(:,:,1) = imadjust(L)*max_luminosity;
frame = applycform(frame, lab2srgb);
imshow(frame);





