% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
imaqhwinfo
imaqhwinfo('winvideo')

vid1winvideo = videoinput('winvideo', 2, 'RGB32_1288x728');
hvpc1 = vision.VideoPlayer;   %create video player objectg
src1winvideo = getselectedsource(vid1winvideo);
vid1winvideo.FramesPerTrigger =1;
vid1winvideo.TriggerRepeat = Inf;
vid1winvideo.ReturnedColorspace = 'rgb';
src1winvideo.FrameRate = '30.0000';   %see propinfo(src,'FrameRate')
start(vid1winvideo)
%start main loop for image acquisition
while 2>1
  imgO=getdata(vid1winvideo,1,'uint8');    %get image from camera
  hvpc1.step(imgO);    %see current image in player
end


imaqhwinfo('gige')
imaqhwinfo('gige',1)
vid2gige = videoinput('gige', 1, 'Mono8');
hvpc2 = vision.VideoPlayer;   %create video player objectg
src2gige = getselectedsource(vid2gige);
vid2gige.FramesPerTrigger =1;
vid2gige.TriggerRepeat = Inf;
vid2gige.ReturnedColorspace = 'rgb';
src2gige.FrameRate = '30.0000';   %see propinfo(src,'FrameRate')
start(vid2gige)

%start main loop for image acquisition
while 2>1
  imgO=getdata(vid2gige,1,'uint8');    %get image from camera
  hvpc2.step(imgO);    %see current image in player
end


vid.ReturnedColorspace = 'mono';

stop

imaqtool .preview(vid2gige)








