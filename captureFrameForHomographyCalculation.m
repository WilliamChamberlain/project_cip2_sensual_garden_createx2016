% https://au.mathworks.com/help/imaq/getting-hardware-information.html
% imaqhwinfo                            % --> adapters
% 'gige'  'winvideo'
% info = imaqhwinfo('winvideo')         % imaqhwinfo(adapter) --> DeviceIDs
% dev_info = imaqhwinfo('winvideo',1)   % imaqhwinfo(adapter , DeviceID)
% info = imaqhwinfo('gige')         % imaqhwinfo(adapter) --> DeviceIDs
% dev_info = imaqhwinfo('gige',1)   % imaqhwinfo(adapter , DeviceID)
% dev_info = imaqhwinfo('gige',2)   % imaqhwinfo(adapter , DeviceID)
% --> detail info

% if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('gige', 1, 'RGB8Packed');  end
if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('gige', 2, 'RGB8Packed');  end
% if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  end
if ~exist('camera_params','var') , camera_params = load_camera_params(); end

[rawImage , metadata] = step(videoDevice);
imshow(rawImage)
rawImageRotated = rawImage;
% rawImageRotated = rot90(rawImage,2);
%firstFrame = undistortImage(firstFrame,camera_params,'cubic','OutputView','full');
undistortedImage = undistortImage(rawImageRotated,camera_params,'OutputView','full');
imshow(undistortedImage)
% Camera_1 - specific
     camera_1_xmin=10;
     camera_1_xmax=1650;
     camera_1_ymin=160;
     camera_1_ymax=915;
% %      firstFrameGoodBits = current_frame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:); 
croppedImage = undistortedImage(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:);
% % croppedImage = undistortedImage;
figure('Name','croppedImage');image(croppedImage);
%% Now find and measure points for the homography
homography_Matrix_ = homography_calculate_and_apply();
%% Now try projecting back the room's midpoint
projected_midpoint = homography_Matrix_\[600,750,1]'; % projected_midpoint = inv(homography_Matrix_)*[600,750,1]';
projected_midpoint = projected_midpoint.*(1/projected_midpoint(3));
figure('Name','croppedImage with room midpoint projected');image(croppedImage); 
    hold on; plot(projected_midpoint(1),projected_midpoint(2),'go');hold off;


% % rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'size',[size(firstFrameGoodBits,1),size(firstFrameGoodBits,2)]);
% [rectifiedImage, offset] = homwarp(homography_Matrix_, croppedImage, 'full'  );
% % rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200);
% rectifiedImage = flipud(rectifiedImage);
% figure('Name','rectifiedImage'); image(rectifiedImage);
%     hold on; plot(projected_midpoint(1),projected_midpoint(2),'go');hold off;

% Camera_1 - specific
camera_1_background_image = croppedImage;
camera_1_background_undistorted_image=croppedImage;
