
Server - Ubuntu

sudo apt-get install libraw1394-11 libgtkmm-2.4-1c2a libglademm-2.4-1c2a libgtkglextmm-x11-1.2-dev libgtkglextmm-x11-1.2 libusb-1.0-0

restart

sudo apt-get install libraw1394-11 libgtkmm-2.4-1c2a libglademm-2.4-1c2a libgtkglextmm-x11-1.2-dev libgtkglextmm-x11-1.2 libusb-1.0-0 libglademm-2.4-dev

restart

[unpack the SDK package to /mnt/nixbig/downloads/flycapture2-2.9.3.43-amd64 ]
cd   /mnt/nixbig/downloads/flycapture2-2.9.3.43-amd64
sudo sh install_flycapture.sh
		agree to all the  IEEE1394 privileges for user 'will'

sudo nano /etc/modules
	add video1394 
	add raw1394
	reboot

physically install ethernet cards
Check in Network Manager - see one ethX per ethernet port (note different from PointGrey doco which implies may have one adapter for several cameras)
Attach network cable - see lights for connection and traffic
Network Manager app
	set up one connection per ethernet port
		each must have a different subnet, which matches the paired camera
		each must have a local IP part which doesn't conflict with the camera
		MTU=9000
		gateway = 0.0.0.0
		DNS = 8.8.8.8
		IPV4 = Manual

Attach camera to Windows 8 laptop with FlyCapture2 installed
Configure cameras	
	0x0014 = 00 00 00 07  for LLA, DHCP, fixed IP
	0x064C = C0 A8 [] [] = 192.168.[subnet].[local] for persistent IP address ( https://www.ptgrey.com/KB/10933 - Step 3—Assign a Persistent IP address)
	0x065C = ff ff ff 00 = 255.255.255.000 for persistent subnet mask
	0x066C = 00 00 00 00 = 000.000.000.000 for Persistent Default Gateway (set this to 0.0.0.0)


Plug cameras into corresponding network ports and check with flycapture2 on Linux


---------------------------------------
----- Configuring through Windows -----
---------------------------------------


Camera 1: new 
0x0014 = 00 00 00 07  for LLA, DHCP, fixed IP
0x064C = C0 A8 16 0C = 192.168.022.012 for persistent IP address ( https://www.ptgrey.com/KB/10933 - Step 3—Assign a Persistent IP address)
0x065C = ff ff ff 00 = 255.255.255.000 for persistent subnet mask
0x066C = 00 00 00 00 = 000.000.000.000 for Persistent Default Gateway (set this to 0.0.0.0)



Camera 2: was configured, now wiped 
0x0014 = 00 00 00 07  for LLA, DHCP, fixed IP
0x064C = C0 A8 16 16 = 192.168.022.022 for persistent IP address ( https://www.ptgrey.com/KB/10933 - Step 3—Assign a Persistent IP address)
0x065C = ff ff ff 00 = 255.255.255.000 for persistent subnet mask
0x066C = 00 00 00 00 = 000.000.000.000 for Persistent Default Gateway (set this to 0.0.0.0)


Camera 3: new 
0x0014 = 00 00 00 07  for LLA, DHCP, fixed IP
0x064C = C0 A8 16 20 = 192.168.022.032 for persistent IP address ( https://www.ptgrey.com/KB/10933 - Step 3—Assign a Persistent IP address)
0x065C = ff ff ff 00 = 255.255.255.000 for persistent subnet mask
0x066C = 00 00 00 00 = 000.000.000.000 for Persistent Default Gateway (set this to 0.0.0.0)

Camera 4: new 
0x0014 = 00 00 00 07  for LLA, DHCP, fixed IP
0x064C = C0 A8 16 2A = 192.168.022.042 for persistent IP address ( https://www.ptgrey.com/KB/10933 - Step 3—Assign a Persistent IP address)
0x065C = ff ff ff 00 = 255.255.255.000 for persistent subnet mask
0x066C = 00 00 00 00 = 000.000.000.000 for Persistent Default Gateway (set this to 0.0.0.0)

Camera 5: new 
0x0014 = 00 00 00 07  for LLA, DHCP, fixed IP
0x064C = C0 A8 16 34 = 192.168.022.052 for persistent IP address ( https://www.ptgrey.com/KB/10933 - Step 3—Assign a Persistent IP address)
0x065C = ff ff ff 00 = 255.255.255.000 for persistent subnet mask
0x066C = 00 00 00 00 = 000.000.000.000 for Persistent Default Gateway (set this to 0.0.0.0)

Camera 6: new 
0x0014 = 00 00 00 07  for LLA, DHCP, fixed IP
0x064C = C0 A8 16 3E = 192.168.022.062 for persistent IP address ( https://www.ptgrey.com/KB/10933 - Step 3—Assign a Persistent IP address)
0x065C = ff ff ff 00 = 255.255.255.000 for persistent subnet mask
0x066C = 00 00 00 00 = 000.000.000.000 for Persistent Default Gateway (set this to 0.0.0.0)






--------------------------------------------------------------------------------------
----- Camera -------------------------------------------------------------------------
--------------------------------------------------------------------------------------

	Package:
	camera: BFLY-PGE-09S2C-CS
	MAC address: 00-B0-9D-EB-C1-64
	downloaded files (setup, drivers, etc):
		PC: /home/will/Downloads/PointGrey_BlackFly/
	POE / power-over-ethernet
	GigE / giga-bit ethernet

	Blackfly-GE-datasheet.pdf : 
	On-camera image processing: color interpolation, gamma, and LUT
	16 MByte frame buffer
	Colour
	Sony ICX692, 1/3", 4.08 ?m
	Global shutter
	30 FPS at 1288 x 728
	0.9MP

	BFLY-PGE-Imaging-Performance.pdf : 
	Measurement	Video Mode 0
	Pixel Clock (MHz)	36
	ADC (Bits)		12-bit
	Quantum Efficiency Blue (% at 470 nm) 51
	Quantum Efficiency Green (% at 525 nm) 54
	Quantum Efficiency Red (% at 640 nm) 46
	Temporal Dark Noise (Read Noise) (e-) 8.82
	Signal to Noise Ratio Maximum (dB) 40.45
	Signal to Noise Ratio Maximum (Bits) 6.72
	Absolute Sensitivity Threshold (?) 18.07
	Saturation Capacity (Well Depth) (e-) 11078
	Dynamic Range (dB) 61.51
	Dynamic Range (Bits) 10.22
	Gain (e-/ADU) 0.24

	Latest FlyCapture2 Viewer
	---
	The FlyCapture Viewer package can be used to view images from our single lens Firewire, GigE, USB2, and USB3 cameras, excluding 'Blackfly S' models (see Spinnaker SDK for BFS software). This Viewer package is a minimal installer which will only install our FlyCap2 demo application as well as the minimum necessary binaries and drivers. For access to the full SDK, documentation, older dlls, and example source code, please download the full SDK.

    FlyCapture 2.9 - Release Notes — 04/28/2016 - 76.274KB
    FlyCap2Viewer_2.9.3.43_x86.exe - FlyCapture 2.9.3.43 Viewer - Windows (32-bit) — 04/24/2016 - 29.1424MB
    FlyCap2Viewer_2.9.3.43_x64.exe - FlyCapture 2.9.3.43 Viewer - Windows (64-bit) — 04/24/2016 - 30.1424MB


	Latest FlyCapture2 Full SDK
	---
	The FlyCapture Full SDK package can be used to work with Point Grey Firewire, GigE, USB2, and USB3 cameras, excluding 'Blackfly S' models (see Spinnaker SDK for BFS software). The complete SDK contains all documentation, example source code, precompiled examples, and libraries required to develop your application using our FlyCapture2 SDK. For a minimal installation which only contains our FlyCap2 camera viewer and minimal drivers and runtimes/libraries, please download the FlyCapture2 Viewer download.

    FlyCapture 2.9 - Release Notes — 04/28/2016 - 76.274KB
    flycapture2-2.9.3.43-i386-pkg.tgz - FlyCapture 2.9.3.43 SDK - Linux Ubuntu (32-bit) — 04/27/2016 - 11.280MB
    flycapture2-2.9.3.43-amd64-pkg.tgz - FlyCapture 2.9.3.43 SDK - Linux Ubuntu (64-bit) — 04/27/2016 - 11.1706MB
    FlyCapture_2.9.3.43_x86.exe - FlyCapture 2.9.3.43 SDK - Windows (32-bit) — 04/24/2016 - 249.1416MB
    FlyCapture_2.9.3.43_x64.exe - FlyCapture 2.9.3.43 SDK - Windows (64-bit) — 04/24/2016 - 251.1416MB
    PGR_GTKmm_x86.exe - FlyCapture 2.9.3.43 - Windows GTK Runtimes (32-bit) — 07/17/2015 - 63.1408MB
    PGR_GTKmm_x64.exe - FlyCapture 2.9.3.43 - Windows GTK Runtimes (64-bit) — 07/17/2015 - 63.896MB
    FlyCapture 2.9.3.43 SDK - ARMHF (32-bit) — 04/27/2016 - 17.308MB
    FlyCapture 2.9.3.43 SDK - ARM64 (64-bit) — 04/27/2016 - 2.1996MB


--------------------------------------------------------------------------------------
----- Camera + Ubuntu
--------------------------------------------------------------------------------------
https://www.ptgrey.com/KB/10548
	Getting Started with FlyCapture 2.x and Linux
	Last Revision Date:: 8/26/2015


	sudo apt-get install libraw1394-11 libgtkmm-2.4-1c2a libglademm-2.4-1c2a libgtkglextmm-x11-1.2-dev libgtkglextmm-x11-1.2 libusb-1.0-0

	The following NEW packages will be installed:
	libatkmm-1.6-dev libcairomm-1.0-dev libglademm-2.4-1c2a libglibmm-2.4-dev  libgtkglext1-dev libgtkglextmm-x11-1.2-0 libgtkglextmm-x11-1.2-dev libgtkglextmm-x11-1.2-doc libgtkmm-2.4-dev libpangomm-1.4-dev  libpangox-1.0-dev libsigc++-2.0-dev
	
	[unpack the SDK package to /mnt/nixbig/downloads/flycapture2-2.9.3.43-amd64 ]

	cd /mnt/nixbig/downloads/flycapture2-2.9.3.43-amd64
	sudo sh install_flycapture.sh
		agree to all the  IEEE1394 privileges for user 'will'

	/etc/modules
	add video1394 
	reboot
	camera not detected

	/etc/modules
	add raw1394
	reboot
	camera not detected

https://www.ptgrey.com/KB/10933
	Setting an IP address for a GigE camera to be recognized in Linux
	Last Revision Date:: 10/29/2015

	temporarily disable reverse path filtering for eth1

	Open FlyCap2
	--> error 
		Error: Failed to load glade file `FlyCapture2GUI_GTK.glade'. Make sure that the file is present.

	read the README /mnt/nixbig/downloads/flycapture2-2.9.3.43-amd64/README
	sudo apt-get install libraw1394-11 libgtkmm-2.4-1c2a libglademm-2.4-1c2a libgtkglextmm-x11-1.2-dev libgtkglextmm-x11-1.2 libusb-1.0-0 libglademm-2.4-dev
		The following NEW packages will be installed:
		 libglade2-dev libglademm-2.4-dev




https://www.ptgrey.com/kb/10059
	Last Revision Date:: 2/10/2016 

OpenCV & BlackFly from 2014
	http://kevinhughes.ca/tutorials/point-grey-blackfly-and-opencv/


Set the ethernet IP address
	http://askubuntu.com/questions/338442/how-to-set-static-ip-address
	created ethernet connection "Ethernet connection PointGrey"
	used the device MAC address 44:8A:5B:F1:5F:E7 (eth0)
	MTU: set to 9000
	no security
	IPv4:
		Manual
		address=169.254.1.67
		netmask=255.255.255.0
		gateway=169.254.1.254
		DNS servers=169.254.1.254

	camera now comes up, but WiFi connection no longer connects
		WiFi connection info
			SSID: Not available 24
			mode: infrastructure
			MAC address D0:7E:35:DB:46:C4 (wlan0)
			MTU: automatic
			IPv4: DHCP
			
TODO: 
	https://www.ptgrey.com/KB/10933
		Step 1—Disable Reverse Path Filtering (RPF)
			as before
		- then connect on the ethernet connection
		- then FlyCap2		
		Step 2—Access GigE Registers	
			set the register for persistent IP
	http://www.freelists.org/post/aravis/initialize-point-grey-blackfly-camera,7
		example persistent IP (e.g. 192.168.22.22), subnet mask (e.g. 255.255.255.0), and default gateway
(e.g. 0.0.0.0)
		write register 0x0014 to let the camera remember to use persistent IP
next time it is connected (e.g. 0x00000007)
	http://www.freelists.org/post/aravis/initialize-point-grey-blackfly-camera,8
		
	
On the command line
	will@will-GE70-2PL:/usr/bin$ GigEConfigCmd 

	
	*** Point Grey GigE Config Utility ***
	
	---> Listing all discoverable cameras...
	
	---> 1 GigE camera(s) discovered
	
	*** Camera 0 ***
	Serial number   : 15450468
	Camera model    : Blackfly BFLY-PGE-09S2C
	Camera vendor   : Point Grey Research
	IP Address      : 169.254.1.68
	Subnet Mask     : 255.255.255.0
	Default Gateway : 0.0.0.0
	Mac Address     : 00:B0:9D:EB:C1:64



--------------------------------------------------------------------------------------
----- Camera + MATLAB
--------------------------------------------------------------------------------------
https://www.ptgrey.com/tan/10898
	Getting Started with MATLAB

--------------------------------------------------------------------------------------
----- Camera + IR --------------------------------------------------------------------
--------------------------------------------------------------------------------------

https://www.ptgrey.com/KB/10080
	Removing the IR filter from a color camera. 

	Maybe us:
		For all other cameras, if users wish to switch the IR filter, the options are:

		    Purchase an M12 micro lens holder (ACC-01-5000 or ACC-01-5001).
		    Purchase a different camera.

		Contact sales@ptgrey.com

		IMPORTANT NOTES:

		    In all cases, removing the IR filter voids the portion of the camera hardware warranty that covers the optical path, as described in your camera's Technical Reference Manual.
		    Removing the IR filter alters the optical path of the camera, and may result in problems obtaining proper focus with your lens.


-----

https://forum.openframeworks.cc/t/infrared-tracking-using-flea3-pointgrey-camera/16590

	"
	My requirements are:

	    - There will be visible light on the stage during tracking
	    - Good tracking image quality and good performances
	    - Track a 6x4m wall area on the stage
	    - IR Camera and Projectors can't be more close than 6m from the stage

	I'm thinking about kind of setup:

	Stage lights during tracking
		Stage lights + Dichroic IR Block Visible pass filter86.

	IR Flooding
		http://www.supercircuits.com/Infrared-Illuminators/IR14589

	IR Camera
	Camera - I'm thinking about Flea3 PointGrey camera USB384, because it has a good resolution (1280 x 1024) and very good FPS performances (150FPS). I read a bunch of post saying that Flea2 Firewire has good performances with OF, but a couple of examples (here and here) in witch Flea3 has some problems.
	IR Pass filter - PointGrey monochrome cameras haven't the IR Block filter, so you have nothing to remove from the camera, but you have to add a IR Pass filter. I'll try a Lee87 or 87c52 filter.
	Lenses - One of these31, according to the final distance from the camera to the tracking area.

	My questions are:

	    - Does anyone used Flea3 USB3 camera without problems on OSX?
	    - Can anyone suggest me the right kind of lens considering a 6x4m wall area on the stage and a distance of the camera of at least 6m?

	"
