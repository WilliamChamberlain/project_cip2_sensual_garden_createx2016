function nmea_test
% 
% http://au.mathworks.com/help/matlab/matlab_external/disconnecting-and-cleaning-up.html
%  fclose(s) - close connection
%  delete(s) - remove from workspace
% http://au.mathworks.com/help/matlab/matlab_external/configuring-communication-settings.html
% - set BaudRate, DataBits, Parity, StopBits, Terminator
% 
% http://au.mathworks.com/help/matlab/ref/serial.html - create a Serial object 
% http://au.mathworks.com/help/matlab/matlab_external/connecting-to-the-device.html
% - connect to the serial port via the Serial object
% obj = serial('port')  - platform-dependent
% obj = serial('port','PropertyName',PropertyValue,...) - platform-dependent
% fopen(s)  -->  s.Status
% 
% List serial devices
% instrhwinfo ('serial')  - takes a few seconds
% Instrument Control Toolbox� function instrhwinfo ('serial')
% serialCom3 = serial('COM3')
% get(serialCom3,{'Type','Name','Port'})
% 
% List Serial object properties which can be set
% set(serialCom3)

% List Serial object properties current values
% get(serialCom3)
% http://au.mathworks.com/help/matlab/matlab_external/getting-started-with-serial-i-o.html#f61191
% 
serialCom3 = instrfind('Type', 'serial', 'Port', 'COM3', 'Tag', '');
func_ = onCleanup(@() myCleanupFun(serialCom3));
try

    serialCom3 = serial('COM3');
    serialCom3.BaudRate = 57600;
    fopen(serialCom3);
    data1 = -5;
    data='';
    while data1 < 120 % isempty(data) %for i = 1:10000 %isempty(strmatch('$GPGLL',data))
        data = fscanf(serialCom3);
        a = strtok(data,',');
        b = strtok(data,',');
        robotId = strtok(data,',');
        msgType = strtok(data,',');
        data1 = strtok(data,',');
        data2 = strtok(data,',');
        data3 = strtok(data,',');
        data4 = strtok(data,',');
        msgSeqNo = strtok(data,',');
        crc = strtok(data,',');
        disp({a,b,robotId,msgType,data1,data2,data3,data4,msgSeqNo,crc});
    end
catch ME
    disp(ME)  ;
end

end

function myCleanupFun(serialCom3)
    fclose(serialCom3); 
    delete(serialCom3); 
end