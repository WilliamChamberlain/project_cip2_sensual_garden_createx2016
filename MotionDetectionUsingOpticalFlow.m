%{
vidDevice = imaq.VideoDevice('winvideo', 1, 'MJPG_320x240', ...
                             'ROI', [1 1 320 240], ...
                             'ReturnedColorSpace', 'rgb', ...
                             'DeviceProperties.Brightness', 130, ...
                             'DeviceProperties.Sharpness', 220);
                         
%}                         

videoFReader = vision.VideoFileReader('/mnt/nixbig/data/project_CIP2_Sensual_Garden_CreateX2016/VREP_sim_vids/0008.avi');
videoInfo = info(videoFReader);
videoSize = videoInfo.VideoSize;
 
                         
% Create a System object to estimate direction and speed of object motion from one video frame to another using optical flow.

optical = vision.OpticalFlow( ...
    'OutputValue', 'Horizontal and vertical components in complex form');


% Initialize the vector field lines.

maxWidth = videoSize(1);    % imaqhwinfo(vidDevice,'MaxWidth');
maxHeight = videoSize(2);   % imaqhwinfo(vidDevice,'MaxHeight');
shapes = vision.ShapeInserter;
shapes.Shape = 'Lines';
shapes.BorderColor = 'white';
r = 1:5:maxHeight;
c = 1:5:maxWidth;
[Y, X] = meshgrid(c,r);


%Create VideoPlayer System objects to display the videos.

hVideoIn = vision.VideoPlayer;
hVideoIn.Name  = 'Original Video';
hVideoOut = vision.VideoPlayer;
hVideoOut.Name  = 'Motion Detected Video';

% --- Stream Acquisition and Processing Loop ---


% Create a processing loop to perform motion detection in the input video. This loop uses the System objects you instantiated above.

% Set up for stream
nFrames = 0;
while ( ~isDone(videoFReader) ) %{nFrames<100  &&       % Process for the first 100 frames.
    % Acquire single frame from imaging device.
    rgbData = step(videoFReader);   % step(vidDevice);

    % Compute the optical flow for that particular frame.
    optFlow = step(optical,rgb2gray(rgbData));

    % Downsample optical flow field.
    optFlow_DS = optFlow(r, c);
    H = imag(optFlow_DS)*50;
    V = real(optFlow_DS)*50;
    
    H = arrayfun( @(x)  ((x+0.1) - abs(x-0.1))/2 , H);
    V = arrayfun( @(x)  ((x+0.1) - abs(x-0.1))/2 , V);

    % Draw lines on top of image
    lines = [Y(:)'; X(:)'; Y(:)'+V(:)'; X(:)'+H(:)'];
    rgb_Out = step(shapes, rgbData,  lines');

    % Send image data to video player
    % Display original video.
    step(hVideoIn, rgbData);
    % Display video along with motion vectors.
    step(hVideoOut, rgb_Out);

    % Increment frame count
    nFrames = nFrames + 1;
end


% Close the file reader and video player.

release(videoFReader);
release(hVideoIn);
release(hVideoOut);