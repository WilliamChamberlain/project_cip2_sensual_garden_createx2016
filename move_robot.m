function move_robot(serial_port_, robot_id_, v_left_, v_right_)
    try

          msgBuffer=zeros(1,10,'uint8');
          msgBuffer(1) = 170;           %   0xAA
          msgBuffer(2) = 85;            %   0x55
          msgBuffer(3) = robot_id_;      %   robotID;
          msgBuffer(4) = 1;             %   msgType;
          msgBuffer(5) = v_left_;       %   d1 - vLeft;
          msgBuffer(6) = v_right_;      %   d2 - vRight;
          msgBuffer(7) = 14;            %   d3 - 
          msgBuffer(8) = 1;             %   d4 - 
                                        %   seqno   - in sendMsg
                                        %   crc     - in sendMsg

          coconutFingerNumber_ = nmea_sendMsg(serial_port_, msgBuffer);
    catch ME
        disp(ME)  ;
    end

end