
function x_ = particle_filter_state_function (x, t, process_covariance_)
    x_ = 0.5*x + 25*x/(1 + x^2) + 8*cos(1.2*(t-1)) + sqrt(process_covariance_)*randn;
end