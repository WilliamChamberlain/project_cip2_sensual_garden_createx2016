function homography_Matrix_ = homography_calculate_and_apply()   

    downloads_dir = 'C:\downloads'; % getenv('DOWNLOADS_DIR');
    
    if isempty(strfind(path,'machine_vision_toolbox-3.4'))
        addpath( genpath( [  '\' 'machine_vision_toolbox-3.4' ] ));
    end        
    if isempty(strfind(path,'matlab_Oxford_robotics_Multiple_View_Geometry_allfns'))
        addpath( genpath( [ downloads_dir '\' 'matlab_Oxford_robotics_Multiple_View_Geometry_allfns' ] ));
    end    
    
% Set Machine Vision Toolbox in the path
% Set Oxford Robotics matlab functions in the path
% C:\downloads\matlab_Oxford_robotics_Multiple_View_Geometry_allfns

%     Uses Oxford Robotics  -  http://www.robots.ox.ac.uk/~vgg/hzbook/code/  -  http://www.robots.ox.ac.uk/~vgg/hzbook/code/vgg_multiview/vgg_H_from_x_lin.m


% PIN are pixel coordinates : for camera 1   
% from  undistorted_image=undistortImage(firstFrame,cameraParams,'OutputView','full');
%
    PIN = [
        800,114;
        747,281;
        493,232;
        602,84;
%         401,188;
%         338,404;
%         788,370;
%         815,284;
%         738,224;
%         704,127;
%         539,139;
%         720,171;
%         279,220;
%         624,102;
%         491,557;
%         915,282;
% %         534,245;
        ]';
% POUT are world coordinates in centimetres : for camera 1 
% 0,0 is at the room corner behind Jared's lighting desk: +ve x is toward
% the door next to the foyer; +ve y is toward the scrim
    POUT = [ 
        300,120;
        150,120;
        150,240;
        300,240;
%         425,895;
%         425,535;
%         842,535;
%         904,655;
%         842,775;
%         842,1015;
%         602,1015;
%         842,895;
%         285,842;
%         734,1112;
%         574,369;
%         1016,644;
% %         600,750
        ]';
    
% figure(5000); hold on;
% plot(PIN(1,:),PIN(2,:),'rx');
% for i = 1:9     
%     text(PIN(1,i), PIN(2,i), int2str(i));
% end    
% 
% figure(5001); hold on;
% plot(POUT(1,:),POUT(2,:),'bx');
% for i = 1:9     
%     text(POUT(1,i), POUT(2,i), int2str(i));
% end    
    
homography_Matrix_ = homography(PIN, POUT);  % http://www.petercorke.com/MVTB/vision.pdf
% imWarped = homwarp(H, image, 'dimension', 1200);

% need to know how big it'll be for consistency
% undistorted_image=undistortImage(firstFrame,cameraParams,'OutputView','valid');
% image(undistorted_image)
% size(undistorted_image)
% ans =
%          798        1614           3

% undistorted_image=undistortImage(firstFrame,cameraParams,'OutputView','full');
% image(undistorted_image)
% size(undistorted_image)% 
% ans =
%          938        1659           3

end

% PIN = [ ...
% 242 , 528 , 880 , 818 ; ...
% 413 , 429 , 261 , 156     ]
