function coconutFingerNumber_ = nmea_write_test_b(serial_port_, robotId_)
% 
% http://au.mathworks.com/help/matlab/matlab_external/disconnecting-and-cleaning-up.html
%  fclose(s) - close connection
%  delete(s) - remove from workspace
% http://au.mathworks.com/help/matlab/matlab_external/configuring-communication-settings.html
% - set BaudRate, DataBits, Parity, StopBits, Terminator
% 
% http://au.mathworks.com/help/matlab/ref/serial.html - create a Serial object 
% http://au.mathworks.com/help/matlab/matlab_external/connecting-to-the-device.html
% - connect to the serial port via the Serial object
% obj = serial('port')  - platform-dependent
% obj = serial('port','PropertyName',PropertyValue,...) - platform-dependent
% fopen(s)  -->  s.Status
% 
% List serial devices
% instrhwinfo ('serial')  - takes a few seconds
% Instrument Control Toolbox� function instrhwinfo ('serial')
% serialCom3 = serial('COM3')
% get(serialCom3,{'Type','Name','Port'})
% 
% List Serial object properties which can be set
% set(serialCom3)

% List Serial object properties current values
% get(serialCom3)
% http://au.mathworks.com/help/matlab/matlab_external/getting-started-with-serial-i-o.html#f61191
% 
% func_ = onCleanup(@() myCleanupFun(serialCom3));

% Jonathan Robert's code:
%     data1 = -5;
%     data='';
%     while data1 < 120 % isempty(data) %for i = 1:10000 %isempty(strmatch('$GPGLL',data))
%         data = fscanf(serialCom3);
%         a = strtok(data,',');
%         b = strtok(data,',');
%         robotId = strtok(data,',');
%         msgType = strtok(data,',');
%         data1 = strtok(data,',');
%         data2 = strtok(data,',');
%         data3 = strtok(data,',');
%         data4 = strtok(data,',');
%         msgSeqNo = strtok(data,',');
%         crc = strtok(data,',');
%         disp({a,b,robotId,msgType,data1,data2,data3,data4,msgSeqNo,crc});
%     end


try

      msgBuffer=zeros(1,10,'uint8');
      msgBuffer(1) = 170;           %   0xAA
      msgBuffer(2) = 85;            %   0x55
      msgBuffer(3) = robotId_;      %   robotID;
      msgBuffer(4) = 1;             %   msgType;
      msgBuffer(5) = 106;           %   d1 - vLeft;
      msgBuffer(6) = 106;           %   d2 - vRight;
      msgBuffer(7) = 14;            %   d3 - 
      msgBuffer(8) = 1;             %   d4 - 
                                    %   seqno   - in sendMsg
                                    %   crc     - in sendMsg
    
      coconutFingerNumber_ = nmea_sendMsg(serial_port_, msgBuffer);
catch ME
    disp(ME)  ;
end

end

function myCleanupFun(serialCom3)
    fclose(serialCom3); 
    delete(serialCom3); 
    closeOpenSerialPorts();
end



function listSerialPortsToConsole() 
    instrfind
end

