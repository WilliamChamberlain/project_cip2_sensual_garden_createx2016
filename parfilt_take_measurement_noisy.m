function z_ = parfilt_take_measurement_noisy (x, measurement_noise_covariance_)
    z_ = x^2/20; % measurement from state
    z_ = z_ + sqrt(measurement_noise_covariance_)*randn; % measurement noise
end
