%%
%{ 
== Status ==
Merge with video_flow_extract1

% TODO - UDP NMEA to Yanto - radius from centre, nonlinear on deadspot, capped near walls
% TODO - UDP NMEA to Jared - x,y,theta,v 
% TODO - XBee to robot control vLeft, vRight
% TODO - XBee to robot servos d1,d2,d3
% TODO - free space detection
% TODO - path planning
% TODO - motion control to path
% TODO - swap out control to hand controller
% TODO - LED control
% TODO - 
% TODO - 
% TODO - human motion blobs
% TODO - UDP NMEA to Jared - motion blob relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - human motion characterisation
% TODO - UDP NMEA to Jared - motion characteristics relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - 
% TODO - different personalities for each robot
% TODO - 
% TODO - alter emotions based on interactions - persistent effects of
interaction aspects + drive to path
% TODO - alter emotions based on point in script
% TODO - 
% TODO - 
%  TODO - mask out the Kinect and other light sources with mid-grey before normalising.
%  TODO - normalise the image intensity before detecting - if performance works out.


== Status ==
Get frames from BlackFly as WinVideo in a loop with loop rate set by
cyclePeriod. 
Improved since previous because the imaq.VideoDevice doesn't tank like the
VideoInput does.

%}

%%

% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
% imaqreset;          % clear the decks
% imageAcquisitionInfo=imaqhwinfo;
% winvideoInfo = imaqhwinfo('winvideo'); winvideoInfoDeviceInfo=winvideoInfo.DeviceInfo; winvideoInfoDeviceInfo1=winvideoInfo.DeviceInfo(1) ; winvideoInfoDeviceInfo2=winvideoInfo.DeviceInfo(2) ; winvideoInfoDeviceInfo2imaqhwinfo=imaqhwinfo('winvideo',2);
% gigeInfo = imaqhwinfo('gige'); gigeInfoDeviceInfo=gigeInfo.DeviceInfo; gigeInfoDeviceInfo1=gigeInfo.DeviceInfo(1);

if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  end
if ~exist('camera_params','var') , camera_params = load_camera_params(); end

cycleHz = 1;
cyclePeriod = 1/cycleHz;                 %   0.1seconds per processing cycle
robotCommandPeriodMs=1000; %1.5s
robotVelocityLeft=10; 
robotVelocityRight=6;
motionRegionsMinimumArea=25;
largeMotionRegionMinimumArea=100;
renderRectifiedImageInvHz = 10;          % render the rectified image every whatever frames; 0 or less to not render
delayTriggerCyclePeriod = cyclePeriod - 0.001;   % sensitivity to cycle lenght / how much faith we have in the system clock
imageOld=zeros(3,3,3); imageOld(:,:,:) = [];
homography_Matrix_ = homography_calculate_and_apply();

% WinVideo 
    [firstFrame , metadata] = step(videoDevice);
    firstFrame = rot90(firstFrame,2);
    firstFrame = undistortImage(firstFrame,camera_params,'OutputView','full');    
    camera_1_xmin=220;
    camera_1_xmax=1440;
    camera_1_xmax=1240;
    camera_1_ymin=215;
    camera_1_ymax=870;
    firstFrameGoodBits = firstFrame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:);
%     rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'full'  );
    rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200  );
    rectifiedImage = flipud(rectifiedImage);
    firstFrame = rectifiedImage;
imageOld = firstFrame;  

midpoint = [ size(firstFrame,2)/2 , size(firstFrame,1)/2 ];
diff_image = zeros(size(firstFrame,1),size(firstFrame,2));

numRobotsToTrack=3;

maxRobotSat = 0.3;
minRobotLuminosity = 0.80;
minRobotSize = 3;

robotLocationBase = midpoint; %clear('robotLocation')
robotLocation = robotLocationBase;
for robot_num=1:numRobotsToTrack-1
    robotLocation = [ robotLocation ; robotLocationBase ];
end


playLoop=true;
liveImageFigureHandle = figure('NumberTitle','on','Name', 'live image', 'Position',[0 490 510 328 ]);
figure(liveImageFigureHandle); imshow(firstFrame,'Border','tight'); drawnow;
occupiedFigureHandle = figure('NumberTitle','on','Name', 'occupied image', 'Position',[0 490+510 510 328 ]);
figure(occupiedFigureHandle); imshow(firstFrame,'Border','tight'); drawnow;
% ButtonH=uicontrol('Parent',occupiedFigureHandle,'Style','pushbutton','String','View Data','Position',[0.0 0.0 50 20],'Units','normalized','Visible','on','Callback', 'toggle(playLoop)');
uicontrol('Style', 'pushbutton', 'Callback', 'uiwait(gcbf)');
drawnow;

frame_num=0;
beforeLoopTic=tic;
% while playLoop && frame_num<40 &&  tic-beforeLoopTic < 10000000

oscUdpConnection = udp('169.254.11.219',9000);
fopen(oscUdpConnection);

while playLoop 
    frame_num = frame_num+1;
    timerval=tic;    
                                                                                                tic
    [firstFrame , metadata] = step(videoDevice);                                                toc
    firstFrame = rot90(firstFrame,2);
    firstFrame = undistortImage(firstFrame,camera_params,'OutputView','full');                  toc   
    camera_1_xmin=220;
    camera_1_xmax=1440;
    camera_1_xmax=1240;
    camera_1_ymin=215;
    camera_1_ymax=870;
    firstFrameGoodBits = firstFrame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:); toc
    rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200  );      toc
    rectifiedImage = flipud(rectifiedImage);
    firstFrame = rectifiedImage;                                                                toc
    
    hsvImg = rgb2hsv(firstFrame);
    whiteGlowingBin = hsvImg(:,:,2) < maxRobotSat & hsvImg(:,:,3) > minRobotLuminosity;
    whiteGlowingBin = bwmorph(whiteGlowingBin, 'close'); % Morphological closing to take care of some of the noisy thresholding - http://stackoverflow.com/questions/28004426/segment-pixels-in-an-image-based-on-colour-matlab
    robotRegionProposals = regionprops(whiteGlowingBin, 'Area', 'Centroid', 'BoundingBox');% Use regionprops to filter based on area, return location of green blocks
    robotRegionProposals(vertcat(robotRegionProposals.Area) < minRobotSize) = []; % Remove every region smaller than minRegionSize
        
        figure(liveImageFigureHandle); imshow(firstFrame);  drawnow;
        
        %% free space / obstacles image
        occupiedSpace     = firstFrame - camera_1_background_image; 
        occupiedSpaceGray = rgb2gray(occupiedSpace);
        occupiedSpaceGray = medfilt2(occupiedSpaceGray, [2 10 ]);   % remove noise
        occupiedSpaceGray = medfilt2(occupiedSpaceGray, [10 2]);    % remove noise
        occupiedSpaceBw   = im2bw(occupiedSpaceGray, 0.05); % convert to 
        figure(occupiedFigureHandle); imshow(occupiedSpaceBw); drawnow;        
        
        %% optical flow image
        if ~isempty(imageOld) % if we have a frame from t-1, start taking differences for optical flow
            diff_image  = firstFrame - imageOld;
            diff_image  = rgb2gray(diff_image);
            imageOld    = firstFrame;                 
            diff_image  = medfilt2(diff_image, [2 10 ]);   % remove noise
            diff_image  = medfilt2(diff_image, [10 2]);    % remove noise
            diff_image_bw = im2bw(diff_image, 0.02); % convert to 
%             figure(3000); imshow(diff_image_bw);
        end
        
        %% robots
        points=zeros(size(robotRegionProposals,1),2);
        for propNum_=1:size(robotRegionProposals,1)
            figure(liveImageFigureHandle); hold on; plot( robotRegionProposals(propNum_).Centroid(1),robotRegionProposals(propNum_).Centroid(2), 'rx','MarkerSize',20); drawnow;
            points(propNum_,1) = robotRegionProposals(propNum_).Centroid(1);
            points(propNum_,2) = robotRegionProposals(propNum_).Centroid(2);             
        end
        
        nearestNeighbours = robotCorners( points, size(firstFrame,2), size(firstFrame,1) ); 
        props_used=[];
        for propNum_=size(robotRegionProposals,1):-1:1
            if sum(props_used==propNum_) > 0 
                continue;
            end;
            propNearestNeighbours = nearestNeighbours(propNum_,(nearestNeighbours(propNum_,:) > 0));
    %             nearestNeighbours_ = robotCornersA( [robotRegionProposals(propNum_).Centroid(1),robotRegionProposals(propNum_).Centroid(2)] , points, size(firstFrame,2), size(firstFrame,1) );
            xsum=0;
            ysum=0;
            for i = 1:size(propNearestNeighbours,2)-1
                props_used=[ props_used propNearestNeighbours(i) propNearestNeighbours(i+1) ];
                plot(...
                    [robotRegionProposals(nearestNeighbours(propNearestNeighbours(i))).Centroid(1),robotRegionProposals(nearestNeighbours(propNearestNeighbours(i+1))).Centroid(1)], ...                    
                    [robotRegionProposals(nearestNeighbours(propNearestNeighbours(i))).Centroid(2),robotRegionProposals(nearestNeighbours(propNearestNeighbours(i+1))).Centroid(2)], ...
                    'c' );
                xsum=xsum+robotRegionProposals(nearestNeighbours(propNearestNeighbours(i))).Centroid(1);
                ysum=ysum+robotRegionProposals(nearestNeighbours(propNearestNeighbours(i))).Centroid(2);
            end            
            xsum=xsum+robotRegionProposals(nearestNeighbours(propNearestNeighbours(size(propNearestNeighbours,2)))).Centroid(1);
            ysum=ysum+robotRegionProposals(nearestNeighbours(propNearestNeighbours(size(propNearestNeighbours,2)))).Centroid(2);
%             plot( ...
%                 [robotRegionProposals(nearestNeighbours(1)).Centroid(1),robotRegionProposals(nearestNeighbours(2)).Centroid(1),robotRegionProposals(nearestNeighbours(3)).Centroid(1),robotRegionProposals(nearestNeighbours(4)).Centroid(1)], ...
%                 [robotRegionProposals(nearestNeighbours(1)).Centroid(2),robotRegionProposals(nearestNeighbours(2)).Centroid(2),robotRegionProposals(nearestNeighbours(3)).Centroid(2),robotRegionProposals(nearestNeighbours(4)).Centroid(2)], ...
%                 'c' );
            xmean=xsum/size(propNearestNeighbours,2); %mean([robotRegionProposals(nearestNeighbours(1)).Centroid(1),robotRegionProposals(nearestNeighbours(2)).Centroid(1),robotRegionProposals(nearestNeighbours(3)).Centroid(1),robotRegionProposals(nearestNeighbours(4)).Centroid(1)]);
            ymean=ysum/size(propNearestNeighbours,2); %mean([robotRegionProposals(nearestNeighbours(1)).Centroid(2),robotRegionProposals(nearestNeighbours(2)).Centroid(2),robotRegionProposals(nearestNeighbours(3)).Centroid(2),robotRegionProposals(nearestNeighbours(4)).Centroid(2)]);
            figure(liveImageFigureHandle); hold on; 
            plot(xmean,ymean,'gx');
            plot(xmean,ymean,'go','MarkerSize',50);
            plot(xmean,ymean,'go','MarkerSize',100);
            plot(xmean,ymean,'go','MarkerSize',150);          
            drawnow;
            
            figure(occupiedFigureHandle); hold on; 
            ymean_int=uint32(ymean);
            xmean_int=uint32(xmean);
            occupiedSpaceBw(ymean_int+25:ymean_int-150, xmean_int-25:xmean_int+25)=0;
            plot([xmean_int-25, xmean_int-25, xmean_int+25, xmean_int+25],[ymean_int+25,ymean_int-150,ymean_int-150,ymean_int+25],'c');
            % figure(occupiedFigureHandle); imshow(occupiedSpaceBw); drawnow;      
            plot(xmean,ymean,'gx');
            plot(xmean,ymean,'go','MarkerSize',50);
            plot(xmean,ymean,'go','MarkerSize',100);
            plot(xmean,ymean,'go','MarkerSize',150);            
            drawnow;
            
%             num_burr_rays = 16;
%             angle=(2*pi)/num_burr_rays;
%             for i=1:num_burr_rays                 
%             end
             image_section_robot_x_min=uint32(xmean)-30; if image_section_robot_x_min<1, image_section_robot_x_min=1; end;
             image_section_robot_x_max=uint32(xmean)+30; if image_section_robot_x_max>size(diff_image_bw,2) , image_section_robot_x_max = size(diff_image_bw,2); end;
             image_section_robot_y_min=uint32(ymean)-30; if image_section_robot_y_min<1, image_section_robot_y_min=1; end;
             image_section_robot_y_max=uint32(ymean)+30; if image_section_robot_y_max>size(diff_image_bw,1) , image_section_robot_y_max = size(diff_image_bw,1); end;
             occupiedSpaceBw( image_section_robot_y_min:image_section_robot_y_max , image_section_robot_x_min:image_section_robot_x_max ) = false;
             for distance=60:10:200  %  TODO TODO - distance to object that is not the robot - need to identify the robot shape and exclude, and/or use the freespace 
                 image_section = [];
                 image_section_x_min=uint32(xmean)-distance; if image_section_x_min<1, image_section_x_min=1; end;
                 image_section_x_max=uint32(xmean)+distance; if image_section_x_max>size(diff_image_bw,2) , image_section_x_max = size(diff_image_bw,2); end;
                 image_section_y_min=uint32(ymean)-distance; if image_section_y_min<1, image_section_y_min=1; end;
                 image_section_y_max=uint32(ymean)+distance; if image_section_y_max>size(diff_image_bw,1) , image_section_y_max = size(diff_image_bw,1); end;
                 image_section = occupiedSpaceBw( image_section_y_min:image_section_y_max , image_section_x_min:image_section_x_max );
%                  image_section( image_section_robot_y_min:image_section_robot_y_max , image_section_robot_x_min:image_section_robot_x_max ) = false;
                 if sum(sum(image_section,1),2)>10
                    display(sprintf('robot %d got an occupied region at %d.', propNum_,distance));
                    oscsend2(oscUdpConnection,'/robot/1/occupied','i',distance);
                    break;
                 end
             end
        end            
        
        if renderRectifiedImageInvHz > 0 && 0 == mod(frame_num,renderRectifiedImageInvHz) % rectify image and display it            
            rectifiedImage = homwarp(homography_Matrix_, firstFrame, 'dimension', 1200);  
            for propNum_=1:size(robotRegionProposals,1)
                robotCoords = [ robotRegionProposals(propNum_).Centroid(1) , robotRegionProposals(propNum_).Centroid(2) , 1];
                rectifiedRobotCoords = inv(homography_Matrix_)*robotCoords';rectifiedRobotCoords=rectifiedRobotCoords.*(1/rectifiedRobotCoords(3));
            end
            %%
        end                
    elapsedTime = toc(timerval);   
    if elapsedTime < delayTriggerCyclePeriod , pause(cyclePeriod-elapsedTime); end; 
end


% release(videoDevice);



