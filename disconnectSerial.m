function disconnectSerial(serialObject_)
try
    if ~strcmp('closed',serialObject_.Status');
        fclose(serialObject_);
        delete(serialObject_);
    end
catch ME
    display(strcat('There was a problem closing the serial object.  ', ME.message));
end
end