function setup_videoDevice()

info_Image_Acquisition_Toolbox_hardware = imaqhwinfo                            % --> adapters
% 'gige'  'winvideo'
info_adaptor_winvideo = imaqhwinfo('winvideo')         % imaqhwinfo(adapter) --> DeviceIDs
try info_device_winvideo_1 = imaqhwinfo('winvideo',1), catch ME, display(ME.message); end;   % imaqhwinfo(adapter , DeviceID)
try info_device_winvideo_2 = imaqhwinfo('winvideo',2), catch ME, display(ME.message); end;   % imaqhwinfo(adapter , DeviceID)
% % info_adaptor_gige = imaqhwinfo('gige')         % imaqhwinfo(adapter) --> DeviceIDs
% % try info_device_gige_1 = imaqhwinfo('gige',1), catch ME, display(ME.message); end;   % imaqhwinfo(adapter , DeviceID)
% % try info_device_gige_2 = imaqhwinfo('gige',2), catch ME, display(ME.message); end;   % imaqhwinfo(adapter , DeviceID)
% --> detail info


if ~exist('camera_params','var') , camera_params = load_camera_params(); end


imaqreset

% % try 
% %     if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('gige', 1, 'RGB8Packed');  end
% % catch ME
% %     display('Exception:')
% %     display(ME.message)
% %     me_caught=ME
% % end
% % 
% % try 
% %     if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('gige', 2, 'RGB8Packed');  end
% % catch ME
% %     display('Exception:')
% %     display(ME.message)
% %     me_caught=ME
% % end

try 
    if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  end  % note: doesn't support mono
catch ME
    display('Exception:')
    display(ME.message)
    me_caught=ME
end

end


