function [x_, y_, theta_] = moveRobot(vLeft_, vRight_, x_, y_, theta_, trackWidth_, period_) 
% Calculates the robot motion in world coordinate frame, 
% with +ve x to the right, +ve y up from the user's POV,
% assuming constant velocity, negligble intertia, instantaneous acceleration
    vMean = (vLeft_+vRight_)/2
    xDiff = vMean* cos(theta_)*period_;
    yDiff = vMean* sin(theta_)*period_;
    thetaDiff = ((vRight_-vLeft_)/trackWidth_)*period_;
    
    x_ = x_ + xDiff;
    y_ = y_ + yDiff;
    theta_ = theta_ + thetaDiff;
end