
points=[
718.4, 517.8;
722.3, 492.9;
759.3, 492.7;
756.4, 517.8;
951.4, 361.2;
962.9, 349.6;
987.9, 355.3;
977.2, 367.8;
];


nearestNeighbours=zeros(size(points,1), 4);
nearestNeighbourVals=inf(size(points,1), 4);
nearestNeighbourVals(:,1)=2001;
nearestNeighbourVals(:,2)=2002;
nearestNeighbourVals(:,3)=2003;
nearestNeighbourVals(:,4)=2004;


for i=1:size(points,1) 
    for j=1:size(points,1)
        if i ~= j
            dist = sqrt( ( points(i,1)-points(j,1) )^2 + ( points(i,2)-points(j,2) )^2); % distance i to j 
            [minVal, minIndices] = min(nearestNeighbours(i,:));
            [maxVal, maxIndices] = max(nearestNeighbourVals(i,:));
            if dist < maxVal && dist < 70
                nearestNeighbours(i,maxIndices) = j;
                nearestNeighbourVals(i,maxIndices) = dist;
            end
        end
    end
end
nearestNeighbours
nearestNeighbourVals