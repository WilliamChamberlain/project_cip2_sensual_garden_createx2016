function nearestNeighbours_ = robotCorners (points_, image_width_, image_height_,max_robot_LED_diff_)
% points=[
% 718.4, 517.8;
% 722.3, 492.9;
% 759.3, 492.7;
% 756.4, 517.8;
% 951.4, 361.2;
% 962.9, 349.6;
% 987.9, 355.3;
% 977.2, 367.8;
% ];
% 
max_dist=sqrt(image_width_^2+image_height_);
max_robot_LED_diff=max_robot_LED_diff_;
nearestNeighbours=zeros(size(points_,1), 4);
nearestNeighbourVals=inf(size(points_,1), 4);
nearestNeighbourVals(:,1) = max_dist+1;
nearestNeighbourVals(:,2) = max_dist+2;
nearestNeighbourVals(:,3) = max_dist+3;
nearestNeighbourVals(:,4) = max_dist+4;
for i=1:size(points_,1)
    for j=1:size(points_,1)
        if i ~= j
            dist = sqrt( ( points_(i,1)-points_(j,1) )^2 + ( points_(i,2)-points_(j,2) )^2); % distance i to j 
            [maxVal, maxIndices] = max(nearestNeighbourVals(i,:));
            if dist < maxVal && dist < max_robot_LED_diff
                nearestNeighbours(i,maxIndices) = j;
                nearestNeighbourVals(i,maxIndices) = dist;
            end
        end
    end
    nearestNeighbours(i,1)=i;
end
nearestNeighbours_=nearestNeighbours;
% nearestNeighbours
% nearestNeighbourVals
end






