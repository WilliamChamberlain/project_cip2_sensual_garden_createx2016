function live_view(video_device_)   
    if 0==nargin
        [status, result] = system('tasklist /FI "imagename eq Point Grey FlyCap2.exe" /fo table /nh');
        if (~isempty(strfind(result, 'FlyCap2.exe')))
            %Display warning, and wait for user to press OK.
            waitfor(warndlg('FlyCap2.exe process is running'));
%             %Terminate FlyCap2.exe process.
%             system('taskkill /f /im "Point Grey FlyCap2.exe"');
        end
        if ~exist('videoDevice','var') , videoDevice_ = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  end
    else
        videoDevice_ = video_device_;
    end    
    if ~exist('homography_Matrix_','var') , homography_Matrix_ = homography_calculate_and_apply(); end
    if ~exist('camera_params','var') , camera_params = load_camera_params(); end
    
    bob = 0;
    figurehandle=figure(10001); 
    uicontrol('Style', 'pushbutton', 'Callback', 'uiwait(gcbf)');
%     uicontrol('Style', 'pushbutton', 'Callback', '{ @toggleBob , bob }');
    
    while 1 > bob
        [current_frame , metadata] = step(videoDevice_);
        current_frame = rot90(current_frame,2);
        current_frame = undistortImage(current_frame,camera_params,'OutputView','full');
        camera_1_xmin=220;
        camera_1_xmax=1440;
        camera_1_xmax=1240;
        camera_1_ymin=215;
        camera_1_ymax=870;
        firstFrameGoodBits = current_frame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:);
%         figure(10000); image(firstFrameGoodBits);
        
        rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200);
        rectifiedImage = flipud(rectifiedImage);
        figure(figurehandle); image(rectifiedImage); drawnow;
    end
    
    
    
    
    function toggleBob(src,eventdata,arg1)
        if arg1 > 1 
            bob = 0;
        else        
            bob = 2;
        end
    end
end
