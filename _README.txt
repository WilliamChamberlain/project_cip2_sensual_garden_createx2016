-----------------------------------------------------------------------------
--- Code ---

BitBucket repository 
- https://bitbucket.org/WilliamChamberlain/project_cip2_sensual_garden_createx2016

Working copy
- H:\Documents\ownCloud\project_CIP2_Sensual_Garden_CreateX2016

Local copy on my desktop PC which has the Git through GitKraken - update this from the working copy with Beyond Compare
- C:\ownCloud_local_backup\project_CIP2_Sensual_Garden_CreateX2016


------------------------------------------------------------------------------

captureFrameForHomographyCalculation.m
homography_calculate_and_apply.m
videoCapture_MATLAB_BlackFly_continuing04_videodevice06.m




C:\downloads\matlab_Consistent_imaging_with_consumer_cameras-Derya-2014.zip\code

C:\downloads\matlab_Consistent_imaging_with_consumer_cameras-Derya-2014.zip

C:\downloads\Robot Toolbox - Peter Corke



-- reference -- 
Copy of onboard controller and handset code - extension of the Robotronica code
- H:\Documents\ownCloud\project_CIP2_Sensual_Garden_CreateX2016\robotronica2015code



-----------------------------------------------------------------------------
--- Data files for application ---
H:\Documents\ownCloud\project_CIP2_Sensual_Garden_CreateX2016\background_images\

H:\Documents\ownCloud\project_CIP2_Sensual_Garden_CreateX2016\BlackFly calibration images\camera1\


-----------------------------------------------------------------------------
--- Third-party applications ---

Matlab extensions and reference code
- C:\downloads\matlab_*

PointGrey camera software
- H:\Documents\ownCloud\project_CIP2_Sensual_Garden_CreateX2016\Downloads_PointGrey_BlackFly\ 

For JavaCV-to-PointGrey-GigE and other future work
- C:\downloads\project_CIP2_Sensual_Garden_CreateX2016


-----------------------------------------------------------------------------
--- simulation --- 
Custom models
- H:\Documents\ownCloud\project_CIP2_Sensual_Garden_CreateX2016\VREP_files\models\

H:\Documents\ownCloud\project_CIP2_Sensual_Garden_CreateX2016\VREP sim\







