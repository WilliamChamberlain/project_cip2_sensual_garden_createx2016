%%
%{ 
== Status ==
Get frames from BlackFly as WinVideo in a loop with loop rate set by
cyclePeriod. 
Improved since previous because the imaq.VideoDevice doesn't tank like the
VideoInput does.

%}

%%

% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
imaqreset;          % clear the decks
imaqhwinfo
winvideoInfo = imaqhwinfo('winvideo')
winvideoInfo.DeviceInfo
winvideoInfo.DeviceInfo(1)
winvideoInfo.DeviceInfo(2)
imaqhwinfo('winvideo',2)

gigeInfo = imaqhwinfo('gige')
gigeInfo.DeviceInfo
gigeInfo.DeviceInfo(1)

release(videoDevice)
delete(videoDevice)
clear('videoDevice')

cyclePeriod = 0.05;                 %   0.1seconds per processing cycle

renderRectifiedImage = false;

% WinVideo
videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  % videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB32_1288x728');

% GigE
% videoDevice = imaq.VideoDevice('gige', 1, 'RGB8Packed');

homography_Matrix_ = homography_calculate_and_apply();

delayTriggerCyclePeriod = cyclePeriod - 0.001;   % sensitivity to cycle lenght / how much faith we have in the system clock

figure(9000); p = plot([0,1],'HitTest','off');  title('Click in figure to quit');

imageOld=zeros(3,3,3); imageOld(:,:,:) = [];

for i = 1:100
    timerval=tic;
    [firstFrame , metadata] = step(videoDevice);
    
        figure(1); image(firstFrame);
        if renderRectifiedImage && 0 == mod(i,5) 
            imWarped = homwarp(homography_Matrix_, firstFrame, 'dimension', 1200);
            figure(2); image(imWarped);
        end
        if ~isempty(imageOld)
            imageDiff = firstFrame - imageOld;
            figure(3); image(imageDiff);
        end
        imageOld = firstFrame;
  
  % TODO - replace with plot objects (patch) 'ButtonDownFcn' callback mapped to a function  -  
  % TODO http://stackoverflow.com/questions/4327075/get-information-from-click-on-any-object-in-the-image-in-matlab
  % TODO ... or use GUI buttons - http://au.mathworks.com/matlabcentral/answers/94141-how-can-i-detect-a-buttonup-event-on-a-uicontrol
     if gco ~= p
         display(' QUITTING ! ')
         release(videoDevice)  
         break;
     end
    elapsedTime = toc(timerval);   
    if elapsedTime < delayTriggerCyclePeriod , pause(cyclePeriod-elapsedTime); end; 
end

release(videoDevice);

%  TODO - mask out the Kinect and other light sources with mid-grey before normalising.

%  TODO - normalise the image intensity before detecting - if performance works out.
%{
frame = getsnapshot(videoDevice);
image(frame);
frame=flip(frame,1); frame=flip(frame,2); image(frame);
frame = getsnapshot(videoDevice);frame=flip(frame,1); frame=flip(frame,2); image(frame);
frame=imadjust(frame); image(frame);
srgb2lab = makecform('srgb2lab');
lab2srgb = makecform('lab2srgb');
frame=applycform(frame, srgb2lab);
max_luminosity = 100;
L = frame(:,:,1)/max_luminosity;
frame(:,:,1) = imadjust(L)*max_luminosity;
frame = applycform(frame, lab2srgb);
imshow(frame);
%}




