function camera_1_background_image_ = camera_1_background_image(videoDevice, homography_Matrix_)
%     if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  end
    [firstFrame , metadata] = step(videoDevice);
    firstFrame = rot90(firstFrame,2);
    if ~exist('camera_params','var') , camera_params = load_camera_params(); end
    firstFrame = undistortImage(firstFrame,camera_params,'OutputView','full');
     camera_1_xmin=10;
     camera_1_xmax=1650;
     camera_1_ymin=160;
     camera_1_ymax=915;
     firstFrameGoodBits = firstFrame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:); 
% %     firstFrameGoodBits = firstFrame;
    rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200);
    rectifiedImage = flipud(rectifiedImage);
    figure(9001); image(rectifiedImage);
    camera_1_background_image_ = firstFrameGoodBits;
end