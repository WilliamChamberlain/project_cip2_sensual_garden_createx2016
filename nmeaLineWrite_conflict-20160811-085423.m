%% Write a NMEA line

%%
%{
%}
function nmeaMessage = nmeaLineWrite(connection_ , commandType_, leftWheelVel_, rightWheelVel_, milliseconds_, reserved10Char1_, reserved10Char2_, reservedInt1_, reservedInt2_, reservedFloat1_, reservedFloat2_)
    nmeaMessage = sprintf('%s,%u,%u,%u,%s,%s,%u,%u,%E6,%E6',commandType_,cast(leftWheelVel_,'uint8'), cast(rightWheelVel_,'uint8'), milliseconds_, reserved10Char1_, reserved10Char2_, reservedInt1_, reservedInt2_, reservedFloat1_, reservedFloat2_);
    nmeaMessage = sprintf('$%s*%s',nmeaMessage,nmeaChecksum(nmeaMessage));
    display(nmeaMessage);
    fprintf( connection_ , nmeaMessage );
end

%% 
function sum_ = nmeaChecksum(msg_)
    checksum = uint8(0);
    for i_char = 1:length(msg_)
        checksum = bitxor(checksum, uint8(msg_(i_char))); 
    end
    checksum = dec2hex(checksum, 2);
    sum_ = checksum;
end