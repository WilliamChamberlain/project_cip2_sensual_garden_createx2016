% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup

vid = videoinput('winvideo', 2, 'RGB32_1288x728');

hvpc = vision.VideoPlayer;   %create video player object

src = getselectedsource(vid);
vid.FramesPerTrigger =1;
vid.TriggerRepeat = Inf;
vid.ReturnedColorspace = 'rgb';
src.FrameRate = '30.0000';   %see propinfo(src,'FrameRate')
start(vid)

%start main loop for image acquisition
for t=1:500
  imgO=getdata(vid,1,'uint8');    %get image from camera
  hvpc.step(imgO);    %see current image in player
end
