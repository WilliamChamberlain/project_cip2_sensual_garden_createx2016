function tree_of_hierarchical_clusters_ = locate_robots(robot_LED_proposals_in_rows_)
    tree_of_hierarchical_clusters_ = linkage(robot_LED_proposals_in_rows_,'ward','squaredeuclidean');
end