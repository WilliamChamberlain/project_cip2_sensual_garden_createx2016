function nearestNeighbours_ = robotCornersA (point_, points_, image_width_, image_height_)
% points_=[
% 718.4, 517.8;
% 722.3, 492.9;
% 759.3, 492.7;
% 756.4, 517.8;
% 951.4, 361.2;
% 962.9, 349.6;
% 987.9, 355.3;
% 977.2, 367.8;
% ];
% 
max_dist=sqrt(image_width_^2+image_height_);
nearestNeighbours=zeros(size(points_,1), 4);
nearestNeighbourVals=inf(size(points_,1), 4);
nearestNeighbourVals(:,1) = max_dist+1;
nearestNeighbourVals(:,2) = max_dist+2;
nearestNeighbourVals(:,3) = max_dist+3;
nearestNeighbourVals(:,4) = max_dist+4;

point_num = -1;

for k=1:1
    if points_(k,1)==point_(1) && points_(k,2)==point_(2)
        point_num = k
    end
end

    for j=1:size(points_,1)
        if point_num ~= j
            dist = sqrt( ( points_(point_num,1)-points_(j,1) )^2 + ( points_(point_num,2)-points_(j,2) )^2); % distance i to j 
            [maxVal, maxIndices] = max(nearestNeighbourVals(point_num,:));
            if dist < maxVal && dist < 70
                nearestNeighbours(point_num,maxIndices) = j;
                nearestNeighbourVals(point_num,maxIndices) = dist;
            end
        end
    end
    nearestNeighbours(point_num,1)=point_num;
    
nearestNeighbours_=nearestNeighbours(point_num);
% nearestNeighbours
% nearestNeighbourVals
end






