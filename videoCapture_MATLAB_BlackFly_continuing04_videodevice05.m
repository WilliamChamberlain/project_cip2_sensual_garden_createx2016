%%
%{ 
== Status ==
Merge with video_flow_extract1

% TODO - UDP NMEA to Yanto - radius from centre, nonlinear on deadspot, capped near walls
% TODO - UDP NMEA to Jared - x,y,theta,v 
% TODO - XBee to robot control vLeft, vRight
% TODO - XBee to robot servos d1,d2,d3
% TODO - free space detection
% TODO - path planning
% TODO - motion control to path
% TODO - swap out control to hand controller
% TODO - LED control
% TODO - 
% TODO - 
% TODO - human motion blobs
% TODO - UDP NMEA to Jared - motion blob relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - human motion characterisation
% TODO - UDP NMEA to Jared - motion characteristics relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - 
% TODO - different personalities for each robot
% TODO - 
% TODO - alter emotions based on interactions - persistent effects of
interaction aspects + drive to path
% TODO - alter emotions based on point in script
% TODO - 
% TODO - 
%  TODO - mask out the Kinect and other light sources with mid-grey before normalising.
%  TODO - normalise the image intensity before detecting - if performance works out.


== Status ==
Get frames from BlackFly as WinVideo in a loop with loop rate set by
cyclePeriod. 
Improved since previous because the imaq.VideoDevice doesn't tank like the
VideoInput does.

%}

%%

% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
% imaqreset;          % clear the decks
% imageAcquisitionInfo=imaqhwinfo;
% winvideoInfo = imaqhwinfo('winvideo'); winvideoInfoDeviceInfo=winvideoInfo.DeviceInfo; winvideoInfoDeviceInfo1=winvideoInfo.DeviceInfo(1) ; winvideoInfoDeviceInfo2=winvideoInfo.DeviceInfo(2) ; winvideoInfoDeviceInfo2imaqhwinfo=imaqhwinfo('winvideo',2);
% gigeInfo = imaqhwinfo('gige'); gigeInfoDeviceInfo=gigeInfo.DeviceInfo; gigeInfoDeviceInfo1=gigeInfo.DeviceInfo(1);

addpath('C:\downloads\machine_vision_toolbox-3.4\rvctools\')

if ~ exist('videoDevice','var') , videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  end
if ~exist('camera_params','var') , camera_params = load_camera_params(); end

cycleHz = 1;
cyclePeriod = 1/cycleHz;                 %   0.1seconds per processing cycle
robotCommandPeriodMs=1000; %1.5s
robotVelocityLeft=10; 
robotVelocityRight=6;
motionRegionsMinimumArea=25;
largeMotionRegionMinimumArea=100;
renderRectifiedImageInvHz = 10;          % render the rectified image every whatever frames; 0 or less to not render
delayTriggerCyclePeriod = cyclePeriod - 0.001;   % sensitivity to cycle lenght / how much faith we have in the system clock
imageOld=zeros(3,3,3); imageOld(:,:,:) = [];
homography_Matrix_ = homography_calculate_and_apply();

% WinVideo 
    [firstFrame , metadata] = step(videoDevice);
    firstFrame = rot90(firstFrame,2);
    firstFrame = undistortImage(firstFrame,camera_params,'OutputView','full');    
    camera_1_xmin=220;
    camera_1_xmax=1440;
    camera_1_xmax=1240;
    camera_1_ymin=215;
    camera_1_ymax=870;
    firstFrameGoodBits = firstFrame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:);
%     rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'full'  );
    rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200  );
    rectifiedImage = flipud(rectifiedImage);
    firstFrame = rectifiedImage;
imageOld = firstFrame;  

midpoint = [ size(firstFrame,2)/2 , size(firstFrame,1)/2 ];
diff_image = zeros(size(firstFrame,1),size(firstFrame,2));

numRobotsToTrack=10;

maxRobotSat = 0.3;
minRobotLuminosity = 0.90;
minRobotSize = 3;
maxRobotLEDSize = 100;


robotLocationBase = [-1.00, -1.00]; % robotLocationBase = midpoint; %clear('robotLocation')
robotLocation = robotLocationBase;
for robot_num=1:numRobotsToTrack-1
    robotLocation = [ robotLocation ; robotLocationBase ];
end
robotLocationHistoryRow = robotLocation;
robotLocationHistory = zeros(2,numRobotsToTrack,2,'double');    % camera x,y / u,v
robotLocationHistory(1,:,:) = robotLocationHistoryRow; 
led_vs_track_history = zeros(2,numRobotsToTrack,2,'uint8');     % LED number - from regions spotted in the image - to the robot track being traced
robotStateHistory = zeros(2,numRobotsToTrack,4,'double');       % x,y,theta,vel


playLoop=true;
% figureHandle = figure; subplot(2,2,1); subimage(firstFrame); drawnow;
% figure; subplot(2,2,2); subimage(firstFrame); drawnow;
% uicontrol('Style', 'pushbutton', 'Callback', 'uiwait(gcbf)');
liveImageFigureHandle = figure('NumberTitle','on','Name', 'live image', 'Position',[0 490 510 328 ]);
figure(liveImageFigureHandle); imshow(firstFrame,'Border','tight'); drawnow;
occupiedFigureHandle = figure('NumberTitle','on','Name', 'occupied image', 'Position',[0 490+510 510 328 ]);
figure(occupiedFigureHandle); imshow(firstFrame,'Border','tight'); drawnow;
% ButtonH=uicontrol('Parent',occupiedFigureHandle,'Style','pushbutton','String','View Data','Position',[0.0 0.0 50 20],'Units','normalized','Visible','on','Callback', 'toggle(playLoop)');
 uicontrol('Style', 'pushbutton', 'Callback', 'uiwait(gcbf)');
drawnow;

frame_num=0;
beforeLoopTic=tic;
% while playLoop && frame_num<40 &&  tic-beforeLoopTic < 10000000

oscUdpConnection = udp('169.254.11.219',9000);
fopen(oscUdpConnection);

while playLoop 
    frame_num = frame_num+1;
    robotLocationHistory(frame_num+1, :, :) = robotLocationHistoryRow; 
    timerval=tic;    
                                                                                                
    [firstFrame , metadata] = step(videoDevice);                                                
    firstFrame = rot90(firstFrame,2);
    firstFrame = undistortImage(firstFrame,camera_params,'OutputView','full');                     
    camera_1_xmin=220;
    camera_1_xmax=1440;
    camera_1_xmax=1240;
    camera_1_ymin=215;
    camera_1_ymax=870;
    firstFrameGoodBits = firstFrame(camera_1_ymin:camera_1_ymax,camera_1_xmin:camera_1_xmax,:); 
    rectifiedImage = homwarp(homography_Matrix_, firstFrameGoodBits, 'dimension', 1200  );      
    rectifiedImage = flipud(rectifiedImage);
    firstFrame = rectifiedImage;                                                                
    
    hsvImg = rgb2hsv(firstFrame);
    whiteGlowingBin = hsvImg(:,:,2) < maxRobotSat & hsvImg(:,:,3) > minRobotLuminosity;
    whiteGlowingBin = bwmorph(whiteGlowingBin, 'close'); % Morphological closing to take care of some of the noisy thresholding - http://stackoverflow.com/questions/28004426/segment-pixels-in-an-image-based-on-colour-matlab
    LEDs = regionprops(whiteGlowingBin, 'Area', 'Centroid', 'BoundingBox');% Use regionprops to filter based on area, return location of green blocks
    LEDs(vertcat(LEDs.Area) < minRobotSize) = []; % Remove every region smaller than minRegionSize
    LEDs(vertcat(LEDs.Area) > maxRobotLEDSize) = [];
        
        figure(liveImageFigureHandle); imshow(firstFrame);  drawnow;    
%         subplot(2,2,1); subimage(firstFrame); drawnow;
        
        %% free space / obstacles image
        occupiedSpace     = firstFrame - camera_1_background_image; 
        occupiedSpaceGray = rgb2gray(occupiedSpace);
        occupiedSpaceGray = medfilt2(occupiedSpaceGray, [2 10 ]);   % remove noise
        occupiedSpaceGray = medfilt2(occupiedSpaceGray, [10 2]);    % remove noise
        occupiedSpaceBw   = im2bw(occupiedSpaceGray, 0.05); % convert to 
        figure(occupiedFigureHandle); imshow(occupiedSpaceBw); drawnow;     
%         subplot(2,2,2); subimage(occupiedSpaceBw); drawnow;
        
        %% optical flow image
        if ~isempty(imageOld) % if we have a frame from t-1, start taking differences for optical flow
            diff_image  = firstFrame - imageOld;
            diff_image  = rgb2gray(diff_image);
            imageOld    = firstFrame;                 
            diff_image  = medfilt2(diff_image, [2 10 ]);   % remove noise
            diff_image  = medfilt2(diff_image, [10 2]);    % remove noise
            diff_image_bw = im2bw(diff_image, 0.02); % convert to 
%             figure(3000); imshow(diff_image_bw);
        end
        
        %% robots
        points=zeros(size(LEDs,1),2);
        for propNum_=1:size(LEDs,1)
        figure(liveImageFigureHandle); hold on; plot( LEDs(propNum_).Centroid(1),LEDs(propNum_).Centroid(2), 'rx','MarkerSize',20); drawnow;   
%         subplot(2,2,1); 
            plot( LEDs(propNum_).Centroid(1),LEDs(propNum_).Centroid(2), 'rx','MarkerSize',20);  
        drawnow;
            
            points(propNum_,1) = LEDs(propNum_).Centroid(1);
            points(propNum_,2) = LEDs(propNum_).Centroid(2);             
        end
        
        LEDs_in_rows = zeros(size(LEDs,1), 2);
        for propNum_=size(LEDs,1):-1:1
            LEDs_in_rows(propNum_,1) = LEDs(propNum_).Centroid(1);
            LEDs_in_rows(propNum_,2) = LEDs(propNum_).Centroid(2);
        end
        nearestLEDs = robotCorners( points, size(firstFrame,2), size(firstFrame,1) ); 
        props_used = [];
        robotNumber = 0;
        %% assign the robot number - consistency of numbering assumes that entries in robotRegionProposals are in consistent order - presumably by coordinate order
        for propNum_=size(LEDs,1):-1:1
            if sum(props_used==propNum_) > 0 , continue; end;	% skip if this LED has already been used
            robotNumber = robotNumber+1;
            nearestLEDsFound = nearestLEDs(propNum_,(nearestLEDs(propNum_,:) > 0));
    %             nearestNeighbours_ = robotCornersA( [robotRegionProposals(propNum_).Centroid(1),robotRegionProposals(propNum_).Centroid(2)] , points, size(firstFrame,2), size(firstFrame,1) );
            xsum=0;  ysum=0;            
            for i = 1:size(nearestLEDsFound,2)-1  
                props_used=[ props_used nearestLEDsFound(i) nearestLEDsFound(i+1) ];
                plot(...
                    [LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(1),LEDs(nearestLEDs(nearestLEDsFound(i+1))).Centroid(1)], ...                    
                    [LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(2),LEDs(nearestLEDs(nearestLEDsFound(i+1))).Centroid(2)], ...
                    'c' );
                xsum=xsum+LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(1);
                ysum=ysum+LEDs(nearestLEDs(nearestLEDsFound(i))).Centroid(2);
            end            
            xsum=xsum+LEDs(nearestLEDs(nearestLEDsFound(size(nearestLEDsFound,2)))).Centroid(1);
            ysum=ysum+LEDs(nearestLEDs(nearestLEDsFound(size(nearestLEDsFound,2)))).Centroid(2);
%             plot( ...
%                 [robotRegionProposals(nearestNeighbours(1)).Centroid(1),robotRegionProposals(nearestNeighbours(2)).Centroid(1),robotRegionProposals(nearestNeighbours(3)).Centroid(1),robotRegionProposals(nearestNeighbours(4)).Centroid(1)], ...
%                 [robotRegionProposals(nearestNeighbours(1)).Centroid(2),robotRegionProposals(nearestNeighbours(2)).Centroid(2),robotRegionProposals(nearestNeighbours(3)).Centroid(2),robotRegionProposals(nearestNeighbours(4)).Centroid(2)], ...
%                 'c' );
            xmean=xsum/size(nearestLEDsFound,2); %mean([robotRegionProposals(nearestNeighbours(1)).Centroid(1),robotRegionProposals(nearestNeighbours(2)).Centroid(1),robotRegionProposals(nearestNeighbours(3)).Centroid(1),robotRegionProposals(nearestNeighbours(4)).Centroid(1)]);
            ymean=ysum/size(nearestLEDsFound,2); %mean([robotRegionProposals(nearestNeighbours(1)).Centroid(2),robotRegionProposals(nearestNeighbours(2)).Centroid(2),robotRegionProposals(nearestNeighbours(3)).Centroid(2),robotRegionProposals(nearestNeighbours(4)).Centroid(2)]);
            robotLocation_now=[xmean,ymean];
            %for i=1:size(numRobotsToTrack,2)% find nearest in history and match them 
            if 1 == frame_num 
                robotLocation(robotNumber,:)=robotLocation_now;
                robotLocationHistory(frame_num,robotNumber,:)=robotLocation_now; 
            else
                euc_dists = pdist2(robotLocation_now,squeeze(robotLocationHistory(frame_num-1,:,:)));
                [ min_euc_dist, min_euc_dist_idx] = min(euc_dists);
                robot_number=min_euc_dist_idx; 
                robotLocation(robot_number,:)=robotLocation_now;
                robotLocationHistory(frame_num,robot_number,:)=robotLocation_now;
                robotLocation_then = robotLocationHistory(frame_num-1,robot_number,:);
                position_diff = robotLocation_now - shiftdim( robotLocation_then , 1);
                vel = norm(position_diff,2);
                if vel < 0.2  % TODO not moving fast, so may just be rotating and LEDs dis/appear so centre jumps and theta jumps, but no guage for rotation without motion commands
                    theta = robotStateHistory(frame_num-1,robot_number,3);
                else
                    theta = atan2(position_diff(2), position_diff(1));
                end
                robotStateHistory(frame_num,robot_number,:)=[ xmean , ymean , theta , vel];
                display( sprintf('/robot/%d/track = %d' , robotNumber , robot_number));                    
                oscsend2(oscUdpConnection,sprintf('/robot/%d/state/',robot_number),'ffff',robotStateHistory(frame_num,robot_number,1),robotStateHistory(frame_num,robot_number,2),robotStateHistory(frame_num,robot_number,3),robotStateHistory(frame_num,robot_number,4));           
                led_vs_track_history(frame_num,robotNumber,1)=propNum_;                             
                led_vs_track_history(frame_num,robotNumber,2)=robot_number;
            end
            
            %% plot zones around the robots
%             subplot(2,2,1);  hold on; 
            figure(liveImageFigureHandle); hold on; 
                plot(xmean,ymean,'rx','MarkerSize',50); plot(xmean,ymean,'go','MarkerSize',50);  plot(xmean,ymean,'go','MarkerSize',100); plot(xmean,ymean,'go','MarkerSize',150);   
            drawnow;
            
            %% OSC for Yanto - robot 'radius' from centre (0) to edge (1); 
            max_distance=sqrt(( size(occupiedSpaceBw,1)/2)^2+(size(occupiedSpaceBw,2)/2)^2); % TODO - move out of loop
            distance_from_centre = sqrt((midpoint(1)-ymean)^2+(midpoint(2)-xmean)^2); % TODO - use real room centre, or real pool centre, rather than image centre
            distance_from_centre = distance_from_centre/max_distance;
            display( sprintf('/robot/%d/radius = %d' , robotNumber , distance_from_centre));
            oscsend2(oscUdpConnection,sprintf('/robot/%d/radius',robotNumber),'i',distance_from_centre);
            
            
            %% OSC for Jared - distance to nearest occupied space - currently in pixels; 
            ymean_int=uint32(ymean);
            xmean_int=uint32(xmean);
            occupiedSpaceBw(ymean_int+25:ymean_int-150, xmean_int-25:xmean_int+25)=0;
%             subplot(2,2,1);  hold on; 
            figure(occupiedFigureHandle); hold on; 
                plot([xmean_int-25, xmean_int-25, xmean_int+25, xmean_int+25],[ymean_int+25,ymean_int-150,ymean_int-150,ymean_int+25],'c');
            % figure(occupiedFigureHandle); imshow(occupiedSpaceBw); drawnow;      
                plot(xmean,ymean,'rx','MarkerSize',50 ); plot(xmean,ymean,'go','MarkerSize',50); plot(xmean,ymean,'go','MarkerSize',100); plot(xmean,ymean,'go','MarkerSize',150);            
            drawnow;
            
%             try using burrs num_burr_rays = 16;  angle=(2*pi)/num_burr_rays;  for  i=1:num_burr_rays, 1=1; end;
                % TODO TODO - homography or scale to angle 
             % bounds for robot silohette - remove the robot from the occupied space image
             % TODO - given the robot state, work out the shape and remove that rather than a whole section
             robot_pix_x_min=uint32(xmean)-40; if robot_pix_x_min<1, robot_pix_x_min=1; end;
             robot_pix_x_max=uint32(xmean)+40; if robot_pix_x_max>size(diff_image_bw,2) , robot_pix_x_max = size(diff_image_bw,2); end;
             robot_pix_y_min=uint32(ymean)-110; if robot_pix_y_min<1, robot_pix_y_min=1; end;
             robot_pix_y_max=uint32(ymean)+30; if robot_pix_y_max>size(diff_image_bw,1) , robot_pix_y_max = size(diff_image_bw,1); end;
             plot([robot_pix_x_min, robot_pix_x_min, robot_pix_x_max ,robot_pix_x_max ], ...
                 [robot_pix_y_min , robot_pix_y_max, robot_pix_y_max, robot_pix_y_min],'c')
             figure(occupiedFigureHandle); hold on; occupiedSpaceBw( robot_pix_y_min:robot_pix_y_max , robot_pix_x_min:robot_pix_x_max ) = false;
             
             for distance=40:5:200  %  TODO TODO - distance to object that is not the robot - need to identify the robot shape and exclude, and/or use the freespace 
                 image_section = [];
                 zone_pix_x_min=uint32(xmean)-distance; if zone_pix_x_min<1, zone_pix_x_min=1; end;
                 zone_pix_x_max=uint32(xmean)+distance; if zone_pix_x_max>size(diff_image_bw,2) , zone_pix_x_max = size(diff_image_bw,2); end;
                 zone_pix_y_min=uint32(ymean)-distance; if zone_pix_y_min<1, zone_pix_y_min=1; end;
                 zone_pix_y_max=uint32(ymean)+distance; if zone_pix_y_max>size(diff_image_bw,1) , zone_pix_y_max = size(diff_image_bw,1); end;
                 image_section = occupiedSpaceBw( zone_pix_y_min:zone_pix_y_max , zone_pix_x_min:zone_pix_x_max );
%                  image_section( image_section_robot_y_min:image_section_robot_y_max , image_section_robot_x_min:image_section_robot_x_max ) = false;
                 if sum(sum(image_section,1),2)>20
                    display(sprintf('robot %d got an occupied region at %d.', propNum_,distance));
                    display( sprintf('/robot/%d/occupied = %d' , robotNumber , distance));
                    oscsend2(oscUdpConnection,sprintf('/robot/%d/occupied',robotNumber),'i',distance);
                    break;
                 end
             end
        end            
        
        
        %%
        if 1 == 0 
        if renderRectifiedImageInvHz > 0 && 0 == mod(frame_num,renderRectifiedImageInvHz) % rectify image and display it            
            rectifiedImage = homwarp(homography_Matrix_, firstFrame, 'dimension', 1200);  
            for propNum_=1:size(LEDs,1)
                robotCoords = [ LEDs(propNum_).Centroid(1) , LEDs(propNum_).Centroid(2) , 1];
                rectifiedRobotCoords = inv(homography_Matrix_)*robotCoords';rectifiedRobotCoords=rectifiedRobotCoords.*(1/rectifiedRobotCoords(3));
            end
            %%
        end       
        end
        
    %% Robot states for Jared to highlight
    
    %% Occupied areas for Jared to highlight 
    for u = 1:20:size(occupiedSpaceBw,2)-20
        for v = 1:20:size(occupiedSpaceBw,1)-20
            if sum(sum(occupiedSpaceBw(v:v+20, u:u+20),2),1) > 2
                iter_=iter_+1;
%                 display(sprintf('/occupied_space/ %d %d : iteration=%d', u , v , iter_));
                oscsend2(oscUdpConnection,sprintf('/occupied_space/'),'ii',u,v);           
            end                
        end
    end
    
    
    elapsedTime = toc(timerval);   
    if elapsedTime < delayTriggerCyclePeriod , pause(cyclePeriod-elapsedTime); end; 
    pause(1);
end


% release(videoDevice);



