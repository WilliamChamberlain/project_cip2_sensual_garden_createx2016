function homography_Matrix_ = homography_calculate_and_apply()   

    downloads_dir = 'C:\downloads'; % getenv('DOWNLOADS_DIR');
    
    if isempty(strfind(path,'machine_vision_toolbox-3.4'))
        addpath( genpath( [  '\' 'machine_vision_toolbox-3.4' ] ));
    end        
    if isempty(strfind(path,'matlab_Oxford_robotics_Multiple_View_Geometry_allfns'))
        addpath( genpath( [ downloads_dir '\' 'matlab_Oxford_robotics_Multiple_View_Geometry_allfns' ] ));
    end    
    
% Set Machine Vision Toolbox in the path
% Set Oxford Robotics matlab functions in the path
% C:\downloads\matlab_Oxford_robotics_Multiple_View_Geometry_allfns

%     Uses Oxford Robotics  -  http://www.robots.ox.ac.uk/~vgg/hzbook/code/  -  http://www.robots.ox.ac.uk/~vgg/hzbook/code/vgg_multiview/vgg_H_from_x_lin.m


% PIN are pixel coordinates : for camera 6
    
    PIN = [ ...
        242 , 528 , 880 , 818 ; ...
        413 , 429 , 261 , 156     ];
% POUT are world coordinates : for camera 6 
    POUT = [ ...
        9.0000  ,  6.0000  ,  3.2000  ,  3.8300 ; ... 
        9.0000  ,  9.0000  ,  7.1600  ,  6.3200     ];

homography_Matrix_ = homography(PIN, POUT);  % http://www.petercorke.com/MVTB/vision.pdf
% imWarped = homwarp(H, image, 'dimension', 1200);

end

% PIN = [ ...
% 242 , 528 , 880 , 818 ; ...
% 413 , 429 , 261 , 156     ]
