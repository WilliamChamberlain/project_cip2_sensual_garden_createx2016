%%
%{ 
== Status ==
Merge with video_flow_extract1

% TODO - UDP NMEA to Yanto - radius from centre, nonlinear on deadspot, capped near walls
% TODO - UDP NMEA to Jared - x,y,theta,v 
% TODO - XBee to robot control vLeft, vRight
% TODO - XBee to robot servos d1,d2,d3
% TODO - free space detection
% TODO - path planning
% TODO - motion control to path
% TODO - swap out control to hand controller
% TODO - LED control
% TODO - 
% TODO - 
% TODO - human motion blobs
% TODO - UDP NMEA to Jared - motion blob relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - human motion characterisation
% TODO - UDP NMEA to Jared - motion characteristics relative to robot - x,y,theta,v
from robot's coordinate frame
% TODO - 
% TODO - different personalities for each robot
% TODO - 
% TODO - alter emotions based on interactions - persistent effects of
interaction aspects + drive to path
% TODO - alter emotions based on point in script
% TODO - 
% TODO - 

== Status ==
Get frames from BlackFly as WinVideo in a loop with loop rate set by
cyclePeriod. 
Improved since previous because the imaq.VideoDevice doesn't tank like the
VideoInput does.

%}

%%

% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
imaqreset;          % clear the decks
imageAcquisitionInfo=imaqhwinfo;
winvideoInfo = imaqhwinfo('winvideo'); winvideoInfoDeviceInfo=winvideoInfo.DeviceInfo; winvideoInfoDeviceInfo1=winvideoInfo.DeviceInfo(1) ; winvideoInfoDeviceInfo2=winvideoInfo.DeviceInfo(2) ; winvideoInfoDeviceInfo2imaqhwinfo=imaqhwinfo('winvideo',2);
gigeInfo = imaqhwinfo('gige'); gigeInfoDeviceInfo=gigeInfo.DeviceInfo; gigeInfoDeviceInfo1=gigeInfo.DeviceInfo(1);

if exist('videoDevice')
    release(videoDevice); delete(videoDevice); clear('videoDevice');
end

cycleHz = 20;
cyclePeriod = 1/cycleHz;                 %   0.1seconds per processing cycle

robotCommandPeriodMs=1000; %1.5s

robotVelocityLeft=10; 
robotVelocityRight=6;

motionRegionsMinimumArea=25;
largeMotionRegionMinimumArea=100;

renderRectifiedImageInvHz = 10;          % render the rectified image every whatever frames; 0 or less to not render

% WinVideo
videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB24_1288x728');  % videoDevice = imaq.VideoDevice('winvideo', 2, 'RGB32_1288x728');

% GigE
% videoDevice = imaq.VideoDevice('gige', 1, 'RGB8Packed');

homography_Matrix_ = homography_calculate_and_apply();

delayTriggerCyclePeriod = cyclePeriod - 0.001;   % sensitivity to cycle lenght / how much faith we have in the system clock

figure(9000); p = plot([0,1],'HitTest','off');  title('Click in figure to quit');

imageOld=zeros(3,3,3); imageOld(:,:,:) = [];


[firstFrame , metadata] = step(videoDevice);
firstFrame = undistortImage(firstFrame,cameraParams,'OutputView','full');
firstFrame = rot90(firstFrame,2);
imageOld = firstFrame;  

midpoint = [ size(firstFrame,2)/2 , size(firstFrame,1)/2 ];

numRobotsToTrack=3;

maxRobotSat = 0.5;
minRobotLuminosity = 0.60;
minRobotSize = 3;

robotLocationBase = [midpoint]; %clear('robotLocation')
robotLocation = robotLocationBase;
for robot_num=1:numRobotsToTrack-1
    robotLocation = [ robotLocation ; robotLocationBase ];
end

imageToHoldLEDview = zeros(size(firstFrame,1),size(firstFrame,2),3);

for frame_num = 1:10
    timerval=tic;
    [firstFrame , metadata] = step(videoDevice);
    firstFrame = rot90(firstFrame,2);
    firstFrame = undistortImage(firstFrame,cameraParams,'OutputView','full');
    
    hsvImg = rgb2hsv(firstFrame);
    whiteGlowingBin = hsvImg(:,:,2) < maxRobotSat & hsvImg(:,:,3) > minRobotLuminosity;
    whiteGlowingBin = bwmorph(whiteGlowingBin, 'close'); % Morphological closing to take care of some of the noisy thresholding - http://stackoverflow.com/questions/28004426/segment-pixels-in-an-image-based-on-colour-matlab
    robotRegionProposals = regionprops(whiteGlowingBin, 'Area', 'Centroid', 'BoundingBox');% Use regionprops to filter based on area, return location of green blocks
    robotRegionProposals(vertcat(robotRegionProposals.Area) < minRobotSize) = []; % Remove every region smaller than minRegionSize
        
        figure(1); image(firstFrame);        
        for propNum_=1:size(robotRegionProposals,1)
            figure(1); hold on; plot( robotRegionProposals(propNum_).Centroid(1),robotRegionProposals(propNum_).Centroid(2), 'rx','MarkerSize',20);
        end
        
        if renderRectifiedImageInvHz > 0 && 0 == mod(frame_num,renderRectifiedImageInvHz) 
            % rectify image and display it
            rectifiedImage = homwarp(homography_Matrix_, firstFrame, 'dimension', 1200);
            figure(2); image(rectifiedImage);
            %  plot possible LED locations
            %%
            for propNum_=1:size(robotRegionProposals,1)
                robotCoords = [ robotRegionProposals(propNum_).Centroid(1) , robotRegionProposals(propNum_).Centroid(2) , 1];
                rectifiedRobotCoords = inv(homography_Matrix_)*robotCoords';rectifiedRobotCoords=rectifiedRobotCoords.*(1/rectifiedRobotCoords(3));
                %rectifiedRobotCoords = rectifiedRobotCoords/100;
                figure(2); hold on; plot( rectifiedRobotCoords(1),rectifiedRobotCoords(2), 'go','MarkerSize',5);
                %text( rectifiedRobotCoords(1)+3,rectifiedRobotCoords(2)+3, str(propNum_));
            end
            %%
        end
        if ~isempty(imageOld) % if we have a frame from t-1, start taking differences for optical flow
            imageDiff = firstFrame - imageOld;
            % 
            if renderRectifiedImageInvHz > 0 && 0 == mod(frame_num,renderRectifiedImageInvHz) 
                imageToHoldLEDview(:,:,1)=whiteGlowingBin;imageToHoldLEDview(:,:,2)=whiteGlowingBin;imageToHoldLEDview(:,:,3)=whiteGlowingBin;
                figure(3); image( homwarp(homography_Matrix_, imageToHoldLEDview, 'dimension', 1200) );  
            end
            %figure(3); image(whiteGlowingBin);
        end
        imageOld = firstFrame;         
        
  % TODO - replace with plot objects (patch) 'ButtonDownFcn' callback mapped to a function  -  
  % TODO http://stackoverflow.com/questions/4327075/get-information-from-click-on-any-object-in-the-image-in-matlab
  % TODO ... or use GUI buttons - http://au.mathworks.com/matlabcentral/answers/94141-how-can-i-detect-a-buttonup-event-on-a-uicontrol
%      if gco ~= p %          display(' QUITTING ! ')%          release(videoDevice)  %          break;%      end
    elapsedTime = toc(timerval);   
    if elapsedTime < delayTriggerCyclePeriod , pause(cyclePeriod-elapsedTime); end; 
end

% figure(2); hold on; plot(578, 317, 'gx');

release(videoDevice);

%  TODO - mask out the Kinect and other light sources with mid-grey before normalising.

%  TODO - normalise the image intensity before detecting - if performance works out.



