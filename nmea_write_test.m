function coconutFingerNumber_ = nmea_write_test(robotId_)
% 
% http://au.mathworks.com/help/matlab/matlab_external/disconnecting-and-cleaning-up.html
%  fclose(s) - close connection
%  delete(s) - remove from workspace
% http://au.mathworks.com/help/matlab/matlab_external/configuring-communication-settings.html
% - set BaudRate, DataBits, Parity, StopBits, Terminator
% 
% http://au.mathworks.com/help/matlab/ref/serial.html - create a Serial object 
% http://au.mathworks.com/help/matlab/matlab_external/connecting-to-the-device.html
% - connect to the serial port via the Serial object
% obj = serial('port')  - platform-dependent
% obj = serial('port','PropertyName',PropertyValue,...) - platform-dependent
% fopen(s)  -->  s.Status 
% 


% List serial devices
% instrhwinfo ('serial')  - takes a few seconds


% Instrument Control Toolbox� function instrhwinfo ('serial')
% serialCom3 = serial('COM3')
% get(serialCom3,{'Type','Name','Port'})
% 
% List Serial object properties which can be set
% set(serialCom3)

% List Serial object properties current values
% get(serialCom3)
% http://au.mathworks.com/help/matlab/matlab_external/getting-started-with-serial-i-o.html#f61191
% 
serialCom3 = instrfind('Type', 'serial', 'Port', 'COM3', 'Tag', '');
func_ = onCleanup(@() myCleanupFun(serialCom3));
try

    serialCom3 = serial('COM3');
    serialCom3.BaudRate = 57600;
    fopen(serialCom3);
%     data1 = -5;
%     data='';
%     while data1 < 120 % isempty(data) %for i = 1:10000 %isempty(strmatch('$GPGLL',data))
%         data = fscanf(serialCom3);
%         a = strtok(data,',');
%         b = strtok(data,',');
%         robotId = strtok(data,',');
%         msgType = strtok(data,',');
%         data1 = strtok(data,',');
%         data2 = strtok(data,',');
%         data3 = strtok(data,',');
%         data4 = strtok(data,',');
%         msgSeqNo = strtok(data,',');
%         crc = strtok(data,',');
%         disp({a,b,robotId,msgType,data1,data2,data3,data4,msgSeqNo,crc});
%     end

      msgBuffer=zeros(1,10,'uint8');
      msgBuffer(1) = 170;
      msgBuffer(2) = 85;
      msgBuffer(3) = robotId_; %robotID;
      msgBuffer(4) = 1; %msgType;
      msgBuffer(5) = 106; %vLeft;
      msgBuffer(6) = 106; %vRight;
      msgBuffer(7) = 14;
      msgBuffer(8) = 1;
%       fwrite(serialCom3,msgBuffer,'ieee-le');
      coconutFingerNumber_ = sendMsg(serialCom3, msgBuffer, uint64(now()*1000000000000.0));
catch ME
    disp(ME)  ;
end

end

function myCleanupFun(serialCom3)
    fclose(serialCom3); 
    delete(serialCom3); 
    closeOpenSerialPorts();
end

function transmitSeqno__ = sendMsg(serialCom3_, msgBuffer_, transmitSeqno_)
    transmitSeqno_=transmitSeqno_+1;
    msgBuffer_(9) = transmitSeqno_;
    msgBuffer_(10) = nmeaChecksum(msgBuffer_);
    fwrite(serialCom3_,msgBuffer_);    
    transmitSeqno__ = transmitSeqno_;
end

function closeOpenSerialPorts() 
    s = instrfind('Port','COM3');
    if isempty(s) 
        s = serial('COM3'); 
    else 
        fclose(s); 
        s = s(1); 
    end
    if isempty(s) 
        s = serial('COM3'); 
    else 
        delete(s); 
        s = s(1); 
    end
end

function listSerialPortsToConsole() 
    instrfind
end

