
function transmitSeqno__ = nmea_sendMsg(serialCom3_, msgBuffer_)    
    persistent seqno_;
        if isempty(seqno_)
            seqno_ = 1;
        end    
      seqno_ = seqno_+1;
    msgBuffer_(9) = seqno_;
    msgBuffer_(10) = nmeaChecksum(msgBuffer_);
    fwrite(serialCom3_,msgBuffer_);    
    transmitSeqno__ = seqno_;
end