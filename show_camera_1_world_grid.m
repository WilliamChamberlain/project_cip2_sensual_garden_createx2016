figure('Name','show_camera_1_world_grid'); imshow(camera_1_background_image); drawnow;
hold on;
for i = 20:40:1100
    for j = 20:40:1500
        projected_point = homography_Matrix_\[i,j,1]'; % projected_midpoint = inv(homography_Matrix_)*[600,750,1]';
        projected_point = projected_point.*(1/projected_point(3));
        plot(projected_point(1), projected_point(2),'gx'); 
        text(projected_point(1), projected_point(2),sprintf('%d,%d',i,j),'Color','r'); 
        drawnow;
    end
end
