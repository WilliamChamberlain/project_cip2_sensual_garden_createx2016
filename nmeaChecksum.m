
function sum_ = nmeaChecksum(msg_)
    checksum = uint8(0);
    for i_char = 1:length(msg_)
        checksum = bitxor(checksum, uint8(msg_(i_char)));
    end
    sum_ = checksum;
end