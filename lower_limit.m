function value_ = lower_limit(env_y_min_, limit_)
    value_ = env_y_min_;
    if env_y_min_ < limit_
        value_ = limit_;
    end
end