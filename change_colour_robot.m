function change_colour_robot(serial_port_, robot_id_, colour_)
    try

          msgBuffer=zeros(1,10,'uint8');
          msgBuffer(1) = 170;           %   0xAA
          msgBuffer(2) = 85;            %   0x55
          msgBuffer(3) = robot_id_;      %   robotID;
          msgBuffer(4) = 120;             %   msgType;
          msgBuffer(5) = colour_;       %   d1 - colour from map: 0 is off, 1 is white, 2 red, 3 green, 4 blue, there are others
          msgBuffer(6) = 0;      %   d2 - ignored
          msgBuffer(7) = 100;            %   d3 - percentage brightness 
          msgBuffer(8) = 40;             %   d4 - 1/40 of a second
                                        %   seqno   - in sendMsg
                                        %   crc     - in sendMsg

          coconutFingerNumber_ = nmea_sendMsg(serial_port_, msgBuffer);
    catch ME
        disp(ME)  ;
    end

end