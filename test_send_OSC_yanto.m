%{
172.20.4.76

10000
%}

udpObj = ...
        udp('169.254.11.219',9000);     % NEED the INSTRUMENT Matlab package for this
    
fopen(udpObj);                          % NEED the INSTRUMENT Matlab package for this
    
oscsend2                                % C:\downloads\Matlab_OSC_oscsend\oscsend2.m

while 1==1
    oscsend2(udpObj,'/accxyz','fff',0.127,0.2,0.3);  % /accxyz = accelerometer XYZ for Yantos phone integration
            % [topic address]  ,  [data format]  ,  [data values 1..n]
end

169.254.11.215