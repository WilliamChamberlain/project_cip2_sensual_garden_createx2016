function close_serial_port(port_name_) 
    s = instrfind('Port',port_name_);
    if isempty(s) 
        s = serial(port_name_); 
    else 
        fclose(s); 
        s = s(1); 
    end
    if isempty(s) 
        s = serial(port_name_); 
    else 
        delete(s); 
        s = s(1); 
    end
end