function value_ = upper_limit(env_y_max_, limit_)
    value_ = env_y_max_;
    if env_y_max_ > limit_
        value_ = limit_;
    end
end