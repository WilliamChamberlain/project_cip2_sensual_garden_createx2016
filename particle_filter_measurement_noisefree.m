function z_ = particle_filter_measurement_noisefree (x_)
    z_ = x_^2/20; % measurement from state
end
