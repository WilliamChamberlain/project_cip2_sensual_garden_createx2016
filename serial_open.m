function serial_port_ = serial_open(port_name_)
    serial_port_ = instrfind('Type', 'serial', 'Port', port_name_, 'Tag', '');
    serial_port_ = serial(port_name_);
    serial_port_.BaudRate = 57600;
    fopen(serial_port_);    
end