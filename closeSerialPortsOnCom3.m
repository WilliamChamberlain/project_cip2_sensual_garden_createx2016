
function closeSerialPortsOnCom3() 
    s = instrfind('Port','COM3');
    if isempty(s) 
        s = serial('COM3'); 
    else 
        fclose(s); 
        s = s(1); 
    end
    if isempty(s) 
        s = serial('COM3'); 
    else 
        delete(s); 
        s = s(1); 
    end
end