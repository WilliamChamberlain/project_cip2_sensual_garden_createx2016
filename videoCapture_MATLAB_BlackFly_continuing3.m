% Thanks to http://stackoverflow.com/questions/8901824/getsnapshot-speedup
imaqhwinfo
winvideoInfo = imaqhwinfo('winvideo')
winvideoInfo.DeviceInfo
winvideoInfo.DeviceInfo(1)
winvideoInfo.DeviceInfo(2)
imaqhwinfo('winvideo',2)
gigeInfo = imaqhwinfo('gige')
gigeInfo.DeviceInfo
gigeInfo.DeviceInfo(1)

% vid = videoinput('winvideo', 2, 'RGB32_1288x728');

vid = videoinput('gige', 1, 'RGB8Packed');
% vid = videoinput('gige', 1, 'Mono8');

hvpc = vision.VideoPlayer;   %create video player object - sets up a window for display

src = getselectedsource(vid); 
framesPerTrigger = vid.FramesPerTrigger
vid.FramesPerTrigger = 1
vid.FramesPerTrigger
% vid.FramesPerTrigger =1;  % defaults to 10 fro the BlackFly
vid.TriggerRepeat = Inf;
vid.TriggerRepeat 
% vid.ReturnedColorspace = 'rgb';
% vid.ReturnedColorspace = 'grayscale'
% src.FrameRate = '30.0000';   %see propinfo(src,'FrameRate')
imaqhelp(vid)
frame = getsnapshot(vid);
        image(frame);
get(vid)                    % generic Matlab Video input object properties http://au.mathworks.com/help/imaq/examples/working-with-properties.html
get(src)                    % srouce object specific properties
imaqhelp(vid,'FrameGrabInterval')
get(vid,'FrameGrabInterval')
vid.FrameGrabInterval
%get(src,'FrameGrabInterval')
get(src,'VideoMode')
src.VideoMode
imaqhelp(src,'VideoMode')
% enumerate values
set(src,'VideoMode') 
set(vid,'FrameGrabInterval')
% set(src,'FrameGrabInterval') 
% set(vid,'FrameGrabInterval',2)
% set(vid,'FrameGrabInterval',1)

start(vid) 
vid.running
% get this warning: 
%   Warning: DeviceLinkThroughputLimit property adjusted to 46472000 by the camera.

%start main loop for image acquisition
while get(vid,'FramesAvailable')<1  %Wait until at least 1 frame is available
    try
        unavailable=1;
    catch ME
        display(ME)
    end
end
hvpc.hide
hvpc.show
figure;
p = plot([0,1],'HitTest','off');
title('Click in figure to quit');
imageOld=zeros(3,3,3);
imageOld(:,:,:) = [];

while 2>1
%   imgO=getdata(vid,1,'uint8');    %get image from camera
    try
        display(vid.FramesAvailable);    
        imgO=getdata(vid, 1);%get image from camera
    catch ME
        display(ME)
    end
 %   for i = 1:size(imgO,4)
    %    hvpc.step(imgO(:,:,:,i) );    %see current image in player
    %     imWarped = homwarp(H, imgO(:,:,:,i), 'dimension', 1200);
        subplot(1,3,1), subimage(imgO(:,:,:,1));
        imWarped = homwarp(H, imgO(:,:,:,1), 'dimension', 1200);
        subplot(1,3,2), subimage(imWarped);
        if ~isempty(imageOld)
    %         imageDiff = imgO(:,:,:,i) - imageOld;
            imageDiff = imgO(:,:,:,1) - imageOld;
            subplot(1,3,3), subimage(imageDiff);
        end
    %     imageOld = imgO(:,:,:,i);
        imageOld = imgO(:,:,:,1);
%     end
  
    if gco ~= p
        stop(vid)  
        break;
    end
    
end
stop(vid);

% release(vid)
vid.FramesAvailable

vid.ReturnedColorspace = 'mono';

stop(vid)

% delete(src)
clear('src')
clear('vid')
delete(hvpc)
clear('hvpc') 
clear('gigeInfo') 
clear('winvideoInfo')
clear('imgO')




frame = getsnapshot(vid);
image(frame);
frame=flip(frame,1); frame=flip(frame,2); image(frame);
frame = getsnapshot(vid);frame=flip(frame,1); frame=flip(frame,2); image(frame);
frame=imadjust(frame); image(frame);
srgb2lab = makecform('srgb2lab');
lab2srgb = makecform('lab2srgb');
frame=applycform(frame, srgb2lab);
max_luminosity = 100;
L = frame(:,:,1)/max_luminosity;
frame(:,:,1) = imadjust(L)*max_luminosity;
frame = applycform(frame, lab2srgb);
imshow(frame);





