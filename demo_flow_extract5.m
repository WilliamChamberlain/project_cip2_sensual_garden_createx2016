%{
frame0=getVideoFrames();
frame1=getVideoFrames(5);
frame2=getVideoFrames(5);
frame3=getVideoFrames(5);

figure; imshow( movieFrames(105).cdata(:,:,1) - movieFrames(103).cdata(:,:,1) )

figure; imshow( (movieFrames(105).cdata(:,:,1) - movieFrames(103).cdata(:,:,1)) - (movieFrames(104).cdata(:,:,1) - movieFrames(102).cdata(:,:,1)) )

A = [ 3 4 ; 3 4];
B = [ 1 5 ; 1 5 ];
C = ((A+B) + abs(A-B))/2
%}

% movieFrames = loadVideoFramesFromFile('/mnt/nixbig/data/project_CIP2_Sensual_Garden_CreateX2016/VREP_sim_vids/0008pruned.mp4',50)

% movieFrames = loadVideoFramesFromFile('/mnt/nixbig/data/project_CIP2_Sensual_Garden_CreateX2016/VREP_sim_vids/0008repruned.ogg', 1000)


%             echoudp('on',4012)
%             udpObj = udp('172.20.4.76',10000);


startFrame=50;
numFrames=150;

motionRegionsMinimumArea=25;
largeMotionRegionMinimumArea=100;

osc_UDP_target_IP_address = '131.181.26.141';
osc_UDP_target_port = 11015;
nmea_UDP_target_IP_address = '172.19.16.202';
nmea_UDP_target_port = 11015;

fid = fopen('D:\_will\owncloud\project_CIP2_Sensual_Garden_CreateX2016\OSCseq_recorded_OSC\direct.xml', 'a+');

%% Output OSC for Yanto - open UDP connection
oscUdpObj = udp(osc_UDP_target_IP_address,osc_UDP_target_port);
fopen(oscUdpObj);
oscsend2(oscUdpObj,'/MinisculeDelights/start','s','start') ;

%% Output NMEA for David - open UDP connection
nmeaUdpObj = udp(nmea_UDP_target_IP_address,nmea_UDP_target_port);
fopen(nmeaUdpObj);       

robotLocation = [0.00, 0.00; 0.00, 0.00; 0.00, 0.00];
numRobotsToTrack=3;

robotVelocityLeft=10; 
robotVelocityRight=6;
robotCommandPeriodMs=1000; %1.5s
    robotIdentifier='30';
    nmeaLineWrite(nmeaUdpObj, 'MGRDC', robotVelocityLeft, robotVelocityRight, robotCommandPeriodMs, robotIdentifier, 'START',0,0,pi,pi/-5.1 );

movieFrames = loadVideoFramesFromFile('D:\data\project_CIP2_Sensual_Garden_CreateX2016\VREP_sim_vids\2016_08_11b.mp4', numFrames, startFrame);
a = movieFrames(1).cdata(:,:,:);
robotLocationHistory = zeros( size(movieFrames,2), numRobotsToTrack , 2, 'double');
robotDirectionHistory = zeros( size(movieFrames,2), numRobotsToTrack , 2, 'double');
erosionStructure = strel('square',5);
bigMotionAverageDirection=0.1;
maxRobotSat = 0.05;
minRobotLuminosity = 0.90;
minRobotSize = 200;

imageDiffs = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
imageDiffs2 = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
imageDiffsSummed = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
figure;
hold on
for frameNum_=1:size(movieFrames,2)
    display(sprintf('iteration %d', frameNum_));
    hsvImg = rgb2hsv(movieFrames(frameNum_).cdata); % Convert image to HSV format
    whiteGlowingBin = hsvImg(:,:,2) < maxRobotSat & hsvImg(:,:,3) > minRobotLuminosity;
    whiteGlowingBin = bwmorph(whiteGlowingBin, 'close'); % Morphological closing to take care of some of the noisy thresholding - http://stackoverflow.com/questions/28004426/segment-pixels-in-an-image-based-on-colour-matlab
    robotRegionProposals = regionprops(whiteGlowingBin, 'Area', 'Centroid', 'BoundingBox'); % Use regionprops to filter based on area, return location of green blocks
    robotRegionProposals(vertcat(robotRegionProposals.Area) < minRobotSize) = []; % Remove every region smaller than minRegionSize
    imbwdifferenceOfImages = whiteGlowingBin;

    if 1 == frameNum_
        for propNum_=1:size(robotRegionProposals,1)
            robotLocationHistory(frameNum_,propNum_,1)=robotRegionProposals(propNum_).Centroid(1);
            robotLocationHistory(frameNum_,propNum_,2)=robotRegionProposals(propNum_).Centroid(2);
        end
    end
    
    if frameNum_ > 1
        imageDiffs(:,:,frameNum_-1)=  medfilt2( sum(movieFrames(frameNum_).cdata,3) - sum(movieFrames(frameNum_-1).cdata,3) , [4 4] );
        imageDiffs(:,:,frameNum_-1) = imerode(imageDiffs(:,:,frameNum_-1),erosionStructure);
        
        % Allocate each proposal to a track - in this case the closest one from the
        % last frame (equal dist goes to last one in robotRegionProposals ordering)
            for propNum_=1:size(robotRegionProposals,1)
                minDistToHistLoc = 1000000;    
                nearestHistLocIndex = -1;
                for histNum=1:size(robotLocationHistory,2) %500 frames, 3 tracks, xy --> 500x3x2
                    distToHistLoc = ...
                        sqrt( ...
                        (robotRegionProposals(propNum_).Centroid(1)-robotLocationHistory(frameNum_-1,histNum,1))^2 ...
                        + ...
                        (robotRegionProposals(propNum_).Centroid(2)-robotLocationHistory(frameNum_-1,histNum,2))^2 ... 
                        );
                    if distToHistLoc < minDistToHistLoc 
                        minDistToHistLoc = distToHistLoc;
                        nearestHistLocIndex = histNum;
                    end
                end
                robotLocationHistory(frameNum_,nearestHistLocIndex,:) = robotRegionProposals(propNum_).Centroid';        
            end
    end
    
    
    if frameNum_ > 2
        imageDiffs2(:,:,frameNum_-2)=  medfilt2( sum(movieFrames(frameNum_).cdata,3) - sum(movieFrames(frameNum_-2).cdata,3) , [4 4] );
        imageDiffsSummed(:,:,frameNum_-2)=  imageDiffs2(:,:,frameNum_-2)+imageDiffs(:,:,frameNum_-1);
        
        % Convert RGB image to chosen color space
RGB = im2double(movieFrames(frameNum_).cdata);
cform = makecform('srgb2lab', 'AdaptedWhitePoint', whitepoint('D65'));
I = applycform(RGB,cform);

% pink shirts
% Define thresholds for channel 1 based on histogram settings
channel1Min = 57.290;
channel1Max = 83.970;
% Define thresholds for channel 2 based on histogram settings
channel2Min = 19.824;
channel2Max = 29.742;
% Define thresholds for channel 3 based on histogram settings
channel3Min = 6.676;
channel3Max = 13.585;
% Create mask based on chosen histogram thresholds
BW = ((I(:,:,1) >= channel1Min ) & (I(:,:,1) <= channel1Max)) & ...
    ((I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max)) & ...
    ((I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max)) ;

% blue shirts
% Define thresholds for channel 1 based on histogram settings
channel1Min = 56.614;
channel1Max = 64.843;
% Define thresholds for channel 2 based on histogram settings
channel2Min = -0.342;
channel2Max = 6.915;
% Define thresholds for channel 3 based on histogram settings
channel3Min = -29.872;
channel3Max = -16.963;
BW = (((I(:,:,1) >= channel1Min ) & (I(:,:,1) <= channel1Max)) & ...
    ((I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max)) & ...
    ((I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max)) ...
    ) | BW;


% green shirts
% Define thresholds for channel 1 based on histogram settings
channel1Min = 64.021;
channel1Max = 78.597;
% Define thresholds for channel 2 based on histogram settings
channel2Min = -30.199;
channel2Max = -10.590;
% Define thresholds for channel 3 based on histogram settings
channel3Min = 12.518;
channel3Max = 26.034;
BW = (((I(:,:,1) >= channel1Min ) & (I(:,:,1) <= channel1Max)) & ...
    ((I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max)) & ...
    ((I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max)) ...
    ) | BW;

imageDiffsSummed(:,:,frameNum_-2)=  BW.*255;
         imageDiffsSummed(:,:,frameNum_-2)=medfilt2(imageDiffsSummed(:,:,frameNum_-2), [2 10 ]);
         imageDiffsSummed(:,:,frameNum_-2)=medfilt2(imageDiffsSummed(:,:,frameNum_-2), [10 2]);
        imbwdifferenceOfImages = im2bw(imageDiffsSummed(:,:,frameNum_-2) , 0.02);

        imshow( movieFrames(frameNum_).cdata );
        for robotNum_ = 1:numRobotsToTrack 
            robotLocation = squeeze(robotLocationHistory(frameNum_,robotNum_,:))';
            robotMotion = squeeze(robotLocationHistory(frameNum_,robotNum_,:)-robotLocationHistory(frameNum_-1,robotNum_,:));
            robotMotion = robotMotion';
            if 0 ~= robotMotion(1) && 0 ~= robotMotion(2)
                robotDirection = robotMotion * (norm(robotMotion)^-1);
                robotDirectionHistory(frameNum_,robotNum_,:) = robotDirection;                
            end 
            robotMotionInWorld = robotLocation+(robotMotion.*50);
            hold on;
            % plot green line for robot motion 
            plot( [ robotLocation(1) robotMotionInWorld(1) ] , [ robotLocation(2) robotMotionInWorld(2) ] ... 
                , 'Color' , [ 0.0 1.0 0.3] , 'Linewidth' , 2 );
            
            motionRegions = regionprops(imbwdifferenceOfImages, 'Area', 'Centroid');        

            %% Medium and large motion regions
            motionRegions(vertcat(motionRegions.Area) < motionRegionsMinimumArea) = [];
            vectors = zeros(length(motionRegions), 2);
            if ~isempty(motionRegions)
                for krobotNum_ = 1:length(motionRegions)
                    motionRegions(krobotNum_).dirVec = motionRegions(krobotNum_).Centroid - robotLocation;
                    motionRegions(krobotNum_).weightedVector = motionRegions(krobotNum_).Area / (sqrt( motionRegions(krobotNum_).dirVec(1)^2 + motionRegions(krobotNum_).dirVec(2)^2 )^1.5);
                    vectors(krobotNum_,:) = [motionRegions(krobotNum_).dirVec(1),motionRegions(krobotNum_).dirVec(2)].*motionRegions(krobotNum_).weightedVector;
                    hold on;
                    % plot pink lines to motion regions
                    % plot( [ robotLocation(1) motionRegions(krobotNum_).Centroid(1) ] , [robotLocation(2) motionRegions(krobotNum_).Centroid(2) ] , 'Color' , [ 1 0.5 0.5] , 'Linewidth' , motionRegions(krobotNum_).weightedVector );
                    % plot pink circles on motion regions
                    viscircles([motionRegions(krobotNum_).Centroid(1) , motionRegions(krobotNum_).Centroid(2)] , 5 ...
                        , 'Color' , [ 1 0.5 0.5], 'Linewidth' , 1);
                    plot( [ robotLocation(1) robotLocation(1)+(motionRegions(krobotNum_).Centroid(1)-robotLocation(1))/5 ] , [robotLocation(2) robotLocation(2)+(motionRegions(krobotNum_).Centroid(2)-robotLocation(2))/5 ] , ...
                        'Color' , [ 1 0.5 0.5] , 'Linewidth' , motionRegions(krobotNum_).weightedVector );
        %             plot( [ robotLocation(1) motionRegions(k).dirVec(1) ] , [robotLocation(2) motionRegions(k).dirVec(2) ] , 'Color' , [ 0.1 1 0.1] , 'Linewidth' , 1 );           
                end
                averageWeight = sum([motionRegions.weightedVector])/length(motionRegions);
                averageVector = createUnitVector(mean(vectors,1));
                hold on;
                % plot blue-ish line to averaged movement vector
                plot( [ robotLocation(1) averageVector(1)*30+robotLocation(1) ] , [robotLocation(2) averageVector(2)*100+robotLocation(2) ] ...
                    , 'Color' , [ 0.2 0.2 1] , 'Linewidth' , 2 );

                %% Large motion regions
                % remove small motion regions, keep big motion regions
                display(length(motionRegions));
                motionRegions(vertcat(motionRegions.Area) < largeMotionRegionMinimumArea) = [];
                display(length(motionRegions));
                if ~isempty(motionRegions)
                    display(sprintf('length(motionRegions)=%d',length(motionRegions)));
                    length_motionRegions = length(motionRegions);
                    display(length_motionRegions)
                    for krobotNum_ = 1:length(motionRegions)
                        motionRegions(krobotNum_).dirVec = motionRegions(krobotNum_).Centroid - robotLocation;
                        % weight is number of pixels/distance^1.5
                        motionRegions(krobotNum_).weightedVector = motionRegions(krobotNum_).Area / (sqrt( motionRegions(krobotNum_).dirVec(1)^2 + motionRegions(krobotNum_).dirVec(2)^2 )^1.5);
                        vectors(krobotNum_,:) = [motionRegions(krobotNum_).dirVec(1),motionRegions(krobotNum_).dirVec(2)].*motionRegions(krobotNum_).weightedVector;
                        hold on;
                        % plot red lines to large motion regions
                        %plot( [ robotLocation(1) motionRegions(krobotNum_).Centroid(1) ] , [robotLocation(2) motionRegions(krobotNum_).Centroid(2) ] ...
                        %    , 'Color' , [ 1 0 0 ] , 'Linewidth' , 1 );                
                        angleOfBlob = angleBetweenVectors( robotDirection , motionRegions(krobotNum_).Centroid );
                        if -45 < angleOfBlob && angleOfBlob < 45 % front quadrant
                            viscircles([motionRegions(krobotNum_).Centroid(1) , motionRegions(krobotNum_).Centroid(2)] , 3, 'Linewidth' , 1);
                            %plot( [ robotLocation(1) motionRegions(krobotNum_).Centroid(1) ] , [robotLocation(2) motionRegions(krobotNum_).Centroid(2) ] , 'Color' , [ 1 0 0 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                            oscsend2(oscUdpObj,sprintf('/1/robot%d/front',robotNum_),'tf',num2str(now),motionRegions(krobotNum_).weightedVector) ;
                        elseif 45 < angleOfBlob && angleOfBlob < 135 % right quadrant
                            viscircles([motionRegions(krobotNum_).Centroid(1) , motionRegions(krobotNum_).Centroid(2)] , 3, 'Linewidth' , 1);
                            %plot( [ robotLocation(1) motionRegions(krobotNum_).Centroid(1) ] , [robotLocation(2) motionRegions(krobotNum_).Centroid(2) ] , 'Color' , [ 0 0.2 1 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                            oscsend2(oscUdpObj,sprintf('/1/robot%d/right',robotNum_),'tf',num2str(now),motionRegions(krobotNum_).weightedVector) ;
                        elseif 135 < angleOfBlob || angleOfBlob > -135 % rear quadrant
                            viscircles([motionRegions(krobotNum_).Centroid(1) , motionRegions(krobotNum_).Centroid(2)] , 3, 'Linewidth' , 1);
                            %plot( [ robotLocation(1) motionRegions(krobotNum_).Centroid(1) ] , [robotLocation(2) motionRegions(krobotNum_).Centroid(2) ] , 'Color' , [ 1 0 0 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                            oscsend2(oscUdpObj,sprintf('/1/robot%d/rear',robotNum_),'tf',num2str(now),motionRegions(krobotNum_).weightedVector) ;
                        else % left quadrant
                            viscircles([motionRegions(krobotNum_).Centroid(1) , motionRegions(krobotNum_).Centroid(2)] , 3, 'Linewidth' , 1);
                            %plot( [ robotLocation(1) motionRegions(krobotNum_).Centroid(1) ] , [robotLocation(2) motionRegions(krobotNum_).Centroid(2) ] , 'Color' , [ 0 1 0.2 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                            oscsend2(oscUdpObj,sprintf('/1/robot%d/left',robotNum_),'tf',num2str(now),motionRegions(krobotNum_).weightedVector) ;
                        end
                    end
                    averageWeight = sum([motionRegions.weightedVector])/length(motionRegions);
                    meanVector = mean(vectors);
                    distanceMean = sqrt(meanVector(1)^2 + meanVector(2)^2);
                    distanceMean = distanceMean/5;
                    bigMotionAverageDirection = (atan2(meanVector(2),meanVector(1))/ (abs(3.1416)+abs(-3.1416)))+0.5;
                    averageVector = createUnitVector(mean(vectors,1));
                    hold on;
                    % plot averaged big motion vector as purple
                    plot( [ robotLocation(1) averageVector(1)*30+robotLocation(1) ] , [robotLocation(2) averageVector(2)*30+robotLocation(2) ] ...
                        , 'Color' , [ 0.5 0.2 0.5] , 'Linewidth' , 2 );
        %             obj = udp('rhost',rport);
                    num2str(now)

                    %% output NMEA for David % Currently just the direction of the average vector to big motion blobs.
                    robotVelocityLeft=round((atan2(meanVector(2),meanVector(1)))*10 , 0 );
                    robotVelocityRight=round((atan2(meanVector(2),meanVector(1)))*5 , 0 );
                    robotCommandPeriodMs=1500; %1.5s
                    nmeaLineWrite(nmeaUdpObj, 'MGRDC', robotVelocityLeft, robotVelocityRight, robotCommandPeriodMs, robotIdentifier, 'RUNNING',0,0,0.0,0.0 );
                end
            end    
        hold on

        %% Plot the robot location
        %for krobotNum_ = 1:length(robotRegionProposals)
            boundBox = repmat(robotRegionProposals(robotNum_).BoundingBox(1:2), 5, 1) + ...
                [0 0; ...
                robotRegionProposals(robotNum_).BoundingBox(3) 0;...
                robotRegionProposals(robotNum_).BoundingBox(3) robotRegionProposals(robotNum_).BoundingBox(4);...
                0 robotRegionProposals(robotNum_).BoundingBox(4);...
                0 0];    
            hold on;
            plot(boundBox(:,1), boundBox(:,2), 'r');
            text(boundBox(4,1)+5, boundBox(4,2)+5, sprintf('%d', robotNum_), 'Color','red');
            hold on;
           % plot( [ robotLocation(1) robotMotionInWorld(1) ] , [ robotLocation(2) robotMotionInWorld(2) ] , 'Color' , [ 0.0 1.0 0.3] , 'Linewidth' , 2 );
        %end

        end
    
    
        %% output OSC for Yanto
        % Currently just the direction of the average vector to big motion
        % blobs.
%         oscsend2(oscUdpObj,'/1/fader/B','tf',num2str(now),bigMotionAverageDirection) ;
        %% Write OSC to file - output as for the OSC message - in OSCseq format because OSCseq is not recording properly 
        fprintf(fid, '			<event beat="%d" color="FF000000" selected="false">\n', frameNum_);
        fprintf(fid, '				<atom type="FLOAT" value="%E16"></atom>\n',bigMotionAverageDirection);
        fprintf(fid, '			</event>\n');

        pause(0.01);
    end    
    
end

%% Send OSC to signal end of process.
oscsend2(oscUdpObj,'/MinisculeDelights/stop','s','stop') ;

%% Write OSC to file - close file
fclose(fid);

%%
robotVelocityLeft=0; robotVelocityRight=0; robotCommandPeriodMs=1000;
nmeaLineWrite(nmeaUdpObj, 'MGRDC', robotVelocityLeft, robotVelocityRight, robotCommandPeriodMs, robotIdentifier, 'FINISHED',0,0,pi,pi/-5.1 );


%% output OSC for Yanto - close UDP connection
fclose(oscUdpObj);

%% output NMEA for David - close UDP connection
fclose(nmeaUdpObj);



