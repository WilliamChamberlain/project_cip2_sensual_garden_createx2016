%{
frame0=getVideoFrames();
frame1=getVideoFrames(5);
frame2=getVideoFrames(5);
frame3=getVideoFrames(5);

figure; imshow( movieFrames(105).cdata(:,:,1) - movieFrames(103).cdata(:,:,1) )

figure; imshow( (movieFrames(105).cdata(:,:,1) - movieFrames(103).cdata(:,:,1)) - (movieFrames(104).cdata(:,:,1) - movieFrames(102).cdata(:,:,1)) )

A = [ 3 4 ; 3 4];
B = [ 1 5 ; 1 5 ];
C = ((A+B) + abs(A-B))/2
%}

% movieFrames = loadVideoFramesFromFile('/mnt/nixbig/data/project_CIP2_Sensual_Garden_CreateX2016/VREP_sim_vids/0008pruned.mp4',50)

% movieFrames = loadVideoFramesFromFile('/mnt/nixbig/data/project_CIP2_Sensual_Garden_CreateX2016/VREP_sim_vids/0008repruned.ogg', 1000)


%             echoudp('on',4012)
%             udpObj = udp('172.20.4.76',10000);

startFrame=40;
numFrames=60;

osc_UDP_target_IP_address = '127.0.0.0';
osc_UDP_target_port = 11015;
nmea_UDP_target_IP_address = '172.19.16.202';
nmea_UDP_target_port = 11015;

fid = fopen('D:\_will\owncloud\project_CIP2_Sensual_Garden_CreateX2016\OSCseq_recorded_OSC\direct.xml', 'a+');

%% Output OSC for Yanto - open UDP connection
oscUdpObj = udp(osc_UDP_target_IP_address,osc_UDP_target_port);
fopen(oscUdpObj);
oscsend2(oscUdpObj,'/MinisculeDelights/start','s','start') ;

%% Output NMEA for David - open UDP connection
nmeaUdpObj = udp(nmea_UDP_target_IP_address,nmea_UDP_target_port);
fopen(nmeaUdpObj);       

robotLocation = [0.00, 0.00];
robotLocationHistory = robotLocation;

robotVelocityLeft=10; 
robotVelocityRight=6;
robotCommandPeriodMs=1000; %1.5s
    robotIdentifier='30';
    nmeaLineWrite(nmeaUdpObj, 'MGRDC', robotVelocityLeft, robotVelocityRight, robotCommandPeriodMs, robotIdentifier, 'START',0,0,pi,pi/-5.1 );

movieFrames = loadVideoFramesFromFile('D:\data\project_CIP2_Sensual_Garden_CreateX2016\VREP_sim_vids\0008repruned.mp4', startFrame, numFrames);

a = movieFrames(1).cdata(:,:,:);
robotLocationHistory = zeros( size(movieFrames,2) , 2, 'double');

erosionStructure = strel('square',5);
% erosionStructure = kcircle(3);

size(movieFrames(1).cdata,1)
size(movieFrames(1).cdata,2)

bigMotionAverageDirection=0.1;

% whiteRange = [];
maxRobotSat = 0.05;
minRobotLuminosity = 0.90;
minRobotSize = 1000;

imageDiffs = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
imageDiffs2 = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
imageDiffsSummed = zeros(size(movieFrames(1).cdata,1), size(movieFrames(1).cdata,2), size(movieFrames,2) , 'uint8');
figure;
hold on
for i=1:size(movieFrames,2)
    display(sprintf('iteration %d', i));
%     % Denoise with a gaussian blur
%     movieFrames(i).cdata = imfilter(movieFrames(i).cdata, fspecial('gaussian', 10, 2));
    % Convert image to HSV format
    hsvImg = rgb2hsv(movieFrames(i).cdata);
%     whiteGlowingBin = hsvImg(:,:,1) > whiteRange(1) & hsvImg(:,:,1) < whiteRange(2) & hsvImg(:,:,2) < maxRobotSat;
    whiteGlowingBin = hsvImg(:,:,2) < maxRobotSat & hsvImg(:,:,3) > minRobotLuminosity;
    whiteGlowingBin = bwmorph(whiteGlowingBin, 'close'); % Morphological closing to take care of some of the noisy thresholding - http://stackoverflow.com/questions/28004426/segment-pixels-in-an-image-based-on-colour-matlab
    % Use regionprops to filter based on area, return location of green blocks
    robotRegionProposals = regionprops(whiteGlowingBin, 'Area', 'Centroid', 'BoundingBox');
    % Remove every region smaller than minRegionSize
    robotRegionProposals(vertcat(robotRegionProposals.Area) < minRobotSize) = [];
%     hold on
%     imshow(whiteGlowingBin);
%     pause(0.01);
    imbw = whiteGlowingBin;
%     imshow(im2bw(sum(movieFrames(i).cdata,3) , 0.80));
%     pause(0.01);
    if ~isempty(robotRegionProposals)        
        robotLocation = robotRegionProposals(1).Centroid;
    end
    robotLocationHistory(i,:) = robotLocation;
    if i > 1
%         imageDiffs(:,:,i-1)=  medfilt2( movieFrames(i).cdata(:,:,1) - movieFrames(i-1).cdata(:,:,1) );
        imageDiffs(:,:,i-1)=  medfilt2( sum(movieFrames(i).cdata,3) - sum(movieFrames(i-1).cdata,3) , [4 4] );
        imageDiffs(:,:,i-1) = imerode(imageDiffs(:,:,i-1),erosionStructure);
%         imshow( imageDiffs(:,:,i-1) );
%         pause(0.01);
    end
    if i > 2
%         imageDiffs2(:,:,i-2)=  medfilt2( movieFrames(i).cdata(:,:,1) - movieFrames(i-2).cdata(:,:,1)  );
        imageDiffs2(:,:,i-2)=  medfilt2( sum(movieFrames(i).cdata,3) - sum(movieFrames(i-2).cdata,3) , [4 4] );
        imageDiffsSummed(:,:,i-2)=  imageDiffs2(:,:,i-2)+imageDiffs(:,:,i-1);
%         imageDiffsSummed(:,:,i-2)=imerode(imageDiffsSummed(:,:,i-2),erosionStructure);
        imageDiffsSummed(:,:,i-2)=medfilt2(imageDiffsSummed(:,:,i-2), [2 10 ]);
        imageDiffsSummed(:,:,i-2)=medfilt2(imageDiffsSummed(:,:,i-2), [10 2]);
%         imageDiffsSummed(:,:,i-2)=  imageDiffs(:,:,i-2)+imageDiffs(:,:,i-1);
% %         imshow( arrayfun( @(x) ((x+255) + abs(x-255))/2 , imageDiffs.Summed(:,:,i-2) ) );
%         im = imageDiffsSummed(:,:,i-2);
%         opened = iopen(im, erosionStructure);
%         closed = iclose(opened, erosionStructure);
        imbw = im2bw(imageDiffsSummed(:,:,i-2) , 0.02);
        imshow( imbw );
%         imshow( closed > 10 );
%         pause(0.01);
        
        robotMotion = robotLocationHistory(i,:)-robotLocationHistory(i-1,:);
        if 0 ~= robotMotion(1) && 0 ~= robotMotion(2)
            robotDirection = robotMotion;                
        end 
        robotMotionInWorld = robotLocation+(robotMotion.*100);
        hold on;
        plot( [ robotLocation(1) robotDirection(1) ] , [ robotLocation(2) robotDirection(2) ] , 'Color' , [ 0.0 1.0 0.3] , 'Linewidth' , 2 );
        
        motionRegions = regionprops(imbw, 'Area', 'Centroid');        
        
        %% Medium and large motion regions
        motionRegions(vertcat(motionRegions.Area) < 100) = [];
        vectors = zeros(length(motionRegions), 2);
        if ~isempty(motionRegions)
            for k = 1:length(motionRegions)
                motionRegions(k).dirVec = motionRegions(k).Centroid - robotLocation;
                motionRegions(k).weightedVector = motionRegions(k).Area / (sqrt( motionRegions(k).dirVec(1)^2 + motionRegions(k).dirVec(2)^2 )^1.5);
                vectors(k,:) = [motionRegions(k).dirVec(1),motionRegions(k).dirVec(2)].*motionRegions(k).weightedVector;
                hold on;
                plot( [ robotLocation(1) motionRegions(k).Centroid(1) ] , [robotLocation(2) motionRegions(k).Centroid(2) ] , 'Color' , [ 1 0.5 0.5] , 'Linewidth' , motionRegions(k).weightedVector );
                viscircles([motionRegions(k).Centroid(1) , motionRegions(k).Centroid(2)] , 5);plot( [ robotLocation(1) motionRegions(k).Centroid(1) ] , [robotLocation(2) motionRegions(k).Centroid(2) ] , 'Color' , [ 1 0.5 0.5] , 'Linewidth' , motionRegions(k).weightedVector );
    %             plot( [ robotLocation(1) motionRegions(k).dirVec(1) ] , [robotLocation(2) motionRegions(k).dirVec(2) ] , 'Color' , [ 0.1 1 0.1] , 'Linewidth' , 1 );           
            end
            averageWeight = sum([motionRegions.weightedVector])/length(motionRegions);
            averageVector = createUnitVector(mean(vectors,1));
            hold on;
            plot( [ robotLocation(1) averageVector(1)*100+robotLocation(1) ] , [robotLocation(2) averageVector(2)*100+robotLocation(2) ] , 'Color' , [ 0.2 0.2 1] , 'Linewidth' , 5 );
    
            %% Large motion regions
            % remove small motion regions, keep big motion regions
            display(length(motionRegions));
            motionRegions(vertcat(motionRegions.Area) < 700) = [];
            display(length(motionRegions));
    
            for k = 1:length(motionRegions)
                motionRegions(k).dirVec = motionRegions(k).Centroid - robotLocation;
                % weight is number of pixels/distance^1.5
                motionRegions(k).weightedVector = motionRegions(k).Area / (sqrt( motionRegions(k).dirVec(1)^2 + motionRegions(k).dirVec(2)^2 )^1.5);
                vectors(k,:) = [motionRegions(k).dirVec(1),motionRegions(k).dirVec(2)].*motionRegions(k).weightedVector;
                hold on;
                plot( [ robotLocation(1) motionRegions(k).Centroid(1) ] , [robotLocation(2) motionRegions(k).Centroid(2) ] , 'Color' , [ 1 0 0 ] , 'Linewidth' , 2 );                
                angleOfBlob = angleBetweenVectors( robotDirection , motionRegions(k).Centroid );
                if -45 < angleOfBlob && angleOfBlob < 45 % front quadrant
                    viscircles([motionRegions(k).Centroid(1) , motionRegions(k).Centroid(2)] , 3);plot( [ robotLocation(1) motionRegions(k).Centroid(1) ] , [robotLocation(2) motionRegions(k).Centroid(2) ] , 'Color' , [ 1 0 0 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                    oscsend2(oscUdpObj,'/1/robot1/front','tf',num2str(now),motionRegions(k).weightedVector) ;
                elseif 45 < angleOfBlob && angleOfBlob < 135 % right quadrant
                    viscircles([motionRegions(k).Centroid(1) , motionRegions(k).Centroid(2)] , 3);plot( [ robotLocation(1) motionRegions(k).Centroid(1) ] , [robotLocation(2) motionRegions(k).Centroid(2) ] , 'Color' , [ 0 0.2 1 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                    oscsend2(oscUdpObj,'/1/robot1/right','tf',num2str(now),motionRegions(k).weightedVector) ;
                elseif 135 < angleOfBlob || angleOfBlob > -135 % rear quadrant
                    viscircles([motionRegions(k).Centroid(1) , motionRegions(k).Centroid(2)] , 3);plot( [ robotLocation(1) motionRegions(k).Centroid(1) ] , [robotLocation(2) motionRegions(k).Centroid(2) ] , 'Color' , [ 1 0 0 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                    oscsend2(oscUdpObj,'/1/robot1/rear','tf',num2str(now),motionRegions(k).weightedVector) ;
                else % left quadrant
                    viscircles([motionRegions(k).Centroid(1) , motionRegions(k).Centroid(2)] , 3);plot( [ robotLocation(1) motionRegions(k).Centroid(1) ] , [robotLocation(2) motionRegions(k).Centroid(2) ] , 'Color' , [ 0 1 0.2 ] , 'Linewidth' , 5); % motionRegions(k).weightedVector );
                    oscsend2(oscUdpObj,'/1/robot1/left','tf',num2str(now),motionRegions(k).weightedVector) ;
                end
    %             plot( [ robotLocation(1) motionRegions(k).dirVec(1) ] , [robotLocation(2) motionRegions(k).dirVec(2) ] , 'Color' , [ 0.1 1 0.1] , 'Linewidth' , 1 );           
            end
            averageWeight = sum([motionRegions.weightedVector])/length(motionRegions);
            meanVector = mean(vectors);
            distanceMean = sqrt(meanVector(1)^2 + meanVector(2)^2);
            distanceMean = distanceMean/5;
            bigMotionAverageDirection = (atan2(meanVector(2),meanVector(1))/ (abs(3.1416)+abs(-3.1416)))+0.5;
            averageVector = createUnitVector(mean(vectors,1));
            hold on;
            % plot averaged big motion vector as purple
            plot( [ robotLocation(1) averageVector(1)*100+robotLocation(1) ] , [robotLocation(2) averageVector(2)*100+robotLocation(2) ] , 'Color' , [ 0.5 0.2 0.5] , 'Linewidth' , 5 );
%             obj = udp('rhost',rport);
            num2str(now)
            
            %% output NMEA for David
            % Currently just the direction of the average vector to big motion
            % blobs.
            robotVelocityLeft=round((atan2(meanVector(2),meanVector(1)))*10 , 0 );
            robotVelocityRight=round((atan2(meanVector(2),meanVector(1)))*5 , 0 );
            robotCommandPeriodMs=1500; %1.5s
            nmeaLineWrite(nmeaUdpObj, 'MGRDC', robotVelocityLeft, robotVelocityRight, robotCommandPeriodMs, robotIdentifier, 'RUNNING',0,0,0.0,0.0 );
        end    
%     image(imbw);
%     axis image
    hold on
    
    %% Plot the robot location
    for k = 1:length(robotRegionProposals)
%         plot(regs(k).Centroid(1), regs(k).Centroid(2), 'cx');
        boundBox = repmat(robotRegionProposals(k).BoundingBox(1:2), 5, 1) + ...
            [0 0; ...
            robotRegionProposals(k).BoundingBox(3) 0;...
            robotRegionProposals(k).BoundingBox(3) robotRegionProposals(k).BoundingBox(4);...
            0 robotRegionProposals(k).BoundingBox(4);...
            0 0];    
%         hold on;
        plot(boundBox(:,1), boundBox(:,2), 'r');
        hold on;
        plot( [ robotLocation(1) robotMotionInWorld(1) ] , [ robotLocation(2) robotMotionInWorld(2) ] , 'Color' , [ 0.0 1.0 0.3] , 'Linewidth' , 2 );
    end
    
    
    end
    
    
    %% output OSC for Yanto
    % Currently just the direction of the average vector to big motion
    % blobs.
    oscsend2(oscUdpObj,'/1/fader/B','tf',num2str(now),bigMotionAverageDirection) ;
    %% Write OSC to file - output as for the OSC message - in OSCseq format because OSCseq is not recording properly 
    fprintf(fid, '			<event beat="%d" color="FF000000" selected="false">\n', i);
    fprintf(fid, '				<atom type="FLOAT" value="%E16"></atom>\n',bigMotionAverageDirection);
    fprintf(fid, '			</event>\n');
    
    pause(0.01);
    
end
%% Send OSC to signal end of process.
oscsend2(oscUdpObj,'/MinisculeDelights/stop','s','stop') ;

%% Write OSC to file - close file
fclose(fid);

%%
robotVelocityLeft=0; robotVelocityRight=0; robotCommandPeriodMs=1000;
nmeaLineWrite(nmeaUdpObj, 'MGRDC', robotVelocityLeft, robotVelocityRight, robotCommandPeriodMs, robotIdentifier, 'FINISHED',0,0,pi,pi/-5.1 );


%% output OSC for Yanto - close UDP connection
fclose(oscUdpObj);

%% output NMEA for David - close UDP connection
fclose(nmeaUdpObj);



